//////// Run with argv[1] === 'once' to run once then quit
//

/* eslint-env node, es6 */
"use strict";
process.title = "    fresh    ";
console.log("\n                                 /////fresh\\\\\\\\\\\n");

var child_process = require("child_process");
var fs = require("fs");
var util = require("util");
var path = require("path");


var rollup = require("rollup");
var rollupBabel = require("rollup-plugin-babel");
var rollupReplace = require("rollup-plugin-replace");
var rollupCommonJS = require("rollup-plugin-commonjs");
var rollupNodeResolve = require("rollup-plugin-node-resolve");

var less = require("less");
var LessAutoPrefix = require("less-plugin-autoprefix");
var lessAutoPrefixer = new LessAutoPrefix({browsers: ["last 2 versions"]});


var options = {

    source: "src/",
    distribute: "dist/",
    productionDistribute: "surge_dist/",
    sourceMap : process.env.NODE_ENV === "production" ? false : true,
    production : process.env.NODE_ENV === "production",
    runAtStart: true,
    quitAfterInitialRun : process.argv[2] === "once",

};

var uglify;

if (options.production) {
    console.log('\n\n/// production!')
    uglify = require("uglify-js")

    options.soundchime = {
        client_id: process.env.SOUNDCHIME_CLIENT_ID,
        default_token: process.env.SOUNDCHIME_DEFAULT_TOKEN,
        redirect_uri: process.env.SOUDNCHIME_REDIRECT_URI
    }

} else {
    uglify = function(){}

    options.soundchime = {
        client_id: 'ae1d5789fd7a578da8ecd31e0581ef00',
        redirect_uri: 'http://127.0.0.1:8080/callback.html',
        default_token: '1-257600-4350356-ae9bd6bf559e60b'
    }
}


(function (options) {
    let srcPath = path.format( path.parse( options.source ) );
    let distPath = path.format( path.parse( options.production ? 
                                           options.productionDistribute 
                                           : options.distribute ) );

    options.src = {
        path : srcPath,
        length : srcPath.split( path.sep ).length
    };

    options.dist = {
        path : distPath,
        length : distPath.split( path.sep ).length
    };
})(options);




//////// Fixes
//

/* fixes is array of objects with params:
 * name    : string    a name for reference
 * match   : regex     matches against relative file names from watcher
 * ignore  : regex     matches files to ignore, ignored files may be
 *      processed by fixes further down the list
 * fixer   : function  returns a file to be written, or falsy
 *      to handle writing on its own (calling fresh.done() on completion)
 * runOnce : bool      true to allow changes to multiple matching files
 *      to only trigger the fix once 
 *
 * files are matched in order and only one fix is matched per file
 */

var fixes = [
    {
        name  : "rollup es6",
        match : /.+\.jsx?$/,
        ignore : /__tests__/,
        fixer: rollupTchime,
        sourceDir: ["js","app","index.js"],
        destDir:["js","index.js"],
        runOnce: true
    },
    {
        name : "less",
        match : /.+\.less$/,
        entry: "tchime.less",
        dest: "tchime.css",
        fixer: renderLess,
        runOnce: true,
        path: "css",
    },
    {
        name: "index.html",
        match: /index(\.min)?\.html$/,
        fixer: indexHtml,
        path: ""
    },
    {
        name: "html",
        match: /.+\.html$/,
        fixer: copyHtml,
        path: ""
    },
    {
        name: "images",
        match: /.+\.svg$/,
    }
];


//////// Util
//

function beepr(count) {
    if (count) {
        process.stderr.write("\u0007");
        setTimeout( beepr.bind(null, count - 1), 500 );
    }
}

function log(file, fresh) {
    /* jshint validthis: true */
    var path = fresh.fixPath(file, this);
    console.log(`>>> ${file}, fixPath: ${path}`);
}

function readdirSyncRecursive(dir) {
    return fs.readdirSync(dir).reduce( (list, file) => {
        return fs.statSync( path.join( dir, file ) ).isDirectory() ? 
                list.concat( readdirSyncRecursive( path.join( dir, file ) ) )
            :   list.concat( path.join( dir, file ) );
            
    }, [])
}

//////// Fixers
//

function renderLess(file, fresh) {
    console.log("Render less");
    var dest = path.join(options.dist.path, "css", this.dest );
    var sourceMapDest = this.dest + ".map";

    less.render( fs.readFileSync( path.join(options.src.path, "less", this.entry ), "utf-8"), 
        {
            plugins: [lessAutoPrefixer],
            sourceMap: options.sourceMap ? {
                sourceMapFileInline: true,
                outputSourceFiles: true,
                sourceMapBasepath: path.join( options.src.path, "less" )
            } : false
        })
        .then( output => { 
            console.log(`${file} > ${dest}`);
            fresh.writeFile(dest, output.css);
            fresh.done()
        }, e => console.log(e) )
}

function copyHtml(file, fresh) {
    /*jshint validthis: true*/
    console.log(`copy html: ${file} fixpath: ${fresh.fixPath(file, this)}`);
    return fs.readFileSync(file, "utf-8");
}


function indexHtml(file, fresh) {

    let filestring

    if (fresh.options.production) {
        if (file.indexOf("min") === -1) {
            setTimeout( () => fresh.done(), 1)
            return false
        } else {
            let filePath = path.join(fresh.options.dist.path, this.path, "index.html");
            console.log(`Copying ${file} to ${filePath}`)
            filestring = fs.readFileSync(file, "utf-8")
            filestring = filestring.replace("__google_analytics_goes_here__",
                             fs.readFileSync("util/googleAnalytics" ))
            fs.writeFile(filePath, filestring, () => fresh.done() )
            return false
        }
    } else {
        filestring = fs.readFileSync(file, "utf-8")

        if (file.indexOf("min") !== -1) {
            filestring = filestring.replace("__google_analytics_goes_here__","")
        }

        return filestring
    }
}

function rollupResolver() {
    return {
        resolveId: ( importee, importer ) => {
            if (!importer) {return null;}
            var resPath = path.resolve(path.dirname(importer), importee);

            if (!path.extname(resPath) ) {
                try { 
                    fs.accessSync(resPath + ".jsx") ;
                    resPath = resPath + ".jsx";
                } catch (e) {
                    try {
                        fs.accessSync(resPath + ".js") ;
                        resPath = resPath + ".js";
                    } catch (e) {
                        return null;
                    }
                }
            }

            return resPath;
        }
    }
}

function roll(main, dest, fresh) {
    main = path.join(process.cwd(), main);
    let then = new Date();
    rollup.rollup({
        entry: main,
        external: ["react","react-dom","redux", "react-redux","react-router"],
        format: "es",
        sourceMap: options.sourceMap, 
        plugins: [
            rollupBabel({
                presets: ["react", "es2015-rollup"],
                exclude: "node_modules/**"
            }),
            rollupResolver(),
            rollupNodeResolve({
                jsnext: true,
                main: true,
                browser: true
            }),
            rollupReplace({ 
                "process.env.NODE_ENV" : JSON.stringify("development"),
                "__client_id_goes_here__" : options.soundchime.client_id,
                "__redirect_uri_goes_here__" : options.soundchime.redirect_uri,
                "__default_token_goes_here__" : options.soundchime.default_token

            }),
            rollupCommonJS({
                include: "node_modules/**",
                sourceMap: options.sourceMap
            }),
        ]
    })
    .then( (bundle) => {

        let rollupOptions = {
                globals: {
                    react : "React",
                    ["react-dom"] : "ReactDOM",
                    ["react-redux"] : "ReactRedux",
                    ["react-router"] : "ReactRouter",
                    redux : "Redux"
                },
                format: "iife"
        }

        if (options.production) {
            console.log('gotta uglify bundle')
            
            try {
                let rollupBundle = bundle.generate(rollupOptions)
                console.log('rollup bundled')

                let res = uglify.minify( rollupBundle.code, {
                    fromString: true,
                    compress : {
                        dead_code: true,
                        drop_debugger: true,
                        conditionals: true,
                        loops: true,
                        unused: true,
                        drop_console: true
                    }

                })
                console.log('bundle uglified')

                let minDest = dest.replace(".js",".min.js")

                fresh.writeFile(minDest, res.code, () => {

                    let now = new Date();
                    console.log(`\n-\n-\nMinified bundle written to ${minDest} ${now - then} ms\n-\n-\n`);
                    beepr(1);

                    fresh.done()
                })
            } catch(err) {
                console.log(`>>> ! Rollup failed for ${main}\n`)
                console.log(err)
                beepr(3); 
            }


        } else {
            bundle.write(Object.assign(rollupOptions, {
                dest: dest,
                sourceMap: options.sourceMap
            }))
            .then( () => {
                let now = new Date();
                console.log(`\n-\n-\nBundle written to ${dest} ${now - then} ms\n-\n-\n`);
                beepr(1);
                fresh.done()
            });
        }
    })
    .catch( rej => { 
        beepr(3); 
        console.log(`>>> ! Rollup failed for ${main}\n${rej.stack}`)
    });
}

function rollupTchime(filename, fresh) {
    /* jshint validthis: true */
    console.log(`React rolling triggered by ${filename}`);
    roll( path.join(fresh.options.src.path, ...this.sourceDir),
            path.join(fresh.options.dist.path, ...this.destDir), fresh );
}

////////// Fresh
//

class Fresh {
    constructor(options) {
        this.changedFiles = [];
        this.timeout = null;
        this.options = options ? options : {};
        this.running = 0;
    }

    done() {
        this.running--;
        
        if (this.running === 0) {
            console.log('\n\nall done\n\n')
            if (this.options.quitAfterInitialRun) {
                if (this.options.production) {
                    console.log('ran in production')
                }
                process.exit()
            }
        }
    }

    run(verbose) {
        if ( verbose === undefined ) verbose = true;

        var time = new Date();

        console.log(`\nRun start: ${time.toTimeString()}`);

        if ( verbose ) {
            console.log("\nFile list:\n" + this.changedFiles.join("\n"));
        }

        console.log("//  //  //  //  //  //  ");

        this.changedFiles.forEach( (relativeFile) => {

            for ( var j = 0; j < fixes.length; j++) {
                
                let fix = fixes[j];


                if (fix.match.exec(relativeFile)) {

                    if (fix.runOnce && fix._ranOnce) {

                        break;

                    }

                    if (fix.ignore && fix.ignore.exec(relativeFile)) {
                            
                        console.log(`\nIgnored file: ${relativeFile}`);
                        continue;
                    }


                    console.log(`\nFile: ${relativeFile} | Fixer: ${fix.name}`);
                 
                    var res = false;

                    if (fix.fixer) {

                        this.running++;
                        // call fixer and set `this` to the fix object in fixes
                        // fresh is injected for path resolution via fresh.fixPath
                        // fresh.done() needs to be called to report completion
                        // for async fixers
                        res = fix.fixer.call( fix, relativeFile, this );

                        if (res) {
                            this.writeFile(this.fixPath(relativeFile, fix), res);
                            this.running--;
                        }

                        fix._ranOnce = true;

                    } else {
                        let path = this.fixPath(relativeFile, fix);

                        console.log(`Copying ${fix.name} to ${path}`);

                        this.copyFile( relativeFile, path );

                        fix._ranOnce = true;

                        break;
                    }

                    break; 
                }
            }
        });

        fixes.forEach( (fix) => {
            fix._ranOnce = false;
        });

        time = new Date()
        console.log(`Run end: ${time.toTimeString()}`);

        this.changedFiles = [];
    }

    start() {
        fs.watch(options.src.path, {recursive: true}, (event, filename) => {
            var fn = path.basename(filename);
            if (fn.indexOf(".") !== -1) {
                if (this.timeout) {
                    clearTimeout(this.timeout);
                }
                this.timeout = setTimeout(this.run.bind(this), 1000);

                filename = path.join(options.src.path, filename);
                if (this.changedFiles.indexOf(filename) === -1) {
                    this.changedFiles.push(filename);
                }
            }
        });

        if ( options.runAtStart ) {
            console.log("Running at start");
            readdirSyncRecursive( options.src.path ).forEach( path => {
                this.changedFiles.push( path );
            });

            this.run(false);
        }
    }

    fixPath(filename, fix) {
        // strip src dir
        filename = filename.split( path.sep )
                           .slice( this.options.src.length )
                           .join( path.sep );


        // Change file extension
        if (fix.ext) { 
            filename = path.join( path.dirname(filename), 
                                 path.basename(filename, path.extname(filename)) + "." + fix.ext);
        }

        // Change file directory to path in options.dist
        // Optionally prefix fix.path
        return (
            typeof fix.path === "string"  ? 
                path.join(this.options.dist.path, fix.path, path.basename(filename)) 
            :   path.join(this.options.dist.path, filename)
        )
    }

    writeFile(dest, fileData, callback) {
        fs.writeFile(dest, fileData, callback || this.writeFileErrHandler)
    }

    writeFileErrHandler(err) {
        if (err) { console.log(err);}
    }
    
    copyFile(from, to) {
        console.log(`Copying from ${from}\n    to ${to}`);
        fs.readFile(from, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                this.writeFile(to, data);
            }
        });
    }
}

var fresh = new Fresh(options);
fresh.start();
