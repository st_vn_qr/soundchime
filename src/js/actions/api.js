import { 
    API_RESOURCE_REQUEST,
    API_RESOURCE_RETRIEVED,
    API_FAVORITE_REQUEST,
    API_FAVORITE_RESPONSE,
    API_CONNECTION
} from "./index"

import {
    //DISCONNECTED,
    CONNECTING,
    READY,
    ERROR
} from "../api/status"

import scAuth from "../api/scAuth"
import bgEffects from "../bgEffects"

import Platforms from "../api/Platforms"

export const resourceRetrieved = (resource, platform, response) => {
    return {
        type : API_RESOURCE_RETRIEVED,
        resource,
        response,
        platform
    }
}

export const retrieveResource = (resource, platform, specifier) => {
    return (dispatch, getState) => {
        let platforms

        let state = getState()

        if ( platform ) {
            platforms = [ platform ]
        } else {
            platforms = Object.keys( Platforms )
        }

        for ( let platform of platforms ) {

            if ( !state.api.hasOwnProperty( platform ) ||
                 !state.api[ platform ].hasOwnProperty( "status" ) ||
                 state.api[ platform ].status !== READY
               ) {

                bgEffects.flash("red")

                console.log(`${platform} not connected yet, doing nothing`)
                continue
            }

            let promise = Platforms[platform].retrieve[resource](specifier)

            promise.then( response => {
                if (response) {
                    if (!response.end) {

                        dispatch( resourceRetrieved(resource,
                                                    platform,
                                                    response)

                            
                        )

                    } else {
                        bgEffects.flash("red")
                        console.log(`Reached end of resource ${resource}` +
                                   ` of platorm ${platform}`)
                    }
                }

            })

            promise.catch( err => {
                console.log( err.stack )
                dispatch( platformConnection( platform, ERROR, undefined, err.message ) )
            })

            dispatch({
                type : API_RESOURCE_REQUEST,
                resource,
                platform
            })
        }
    }
}


export const connectPlatform = (useDefault, platform) => {
    return dispatch => {
        let platforms

        if ( platform && !Array.isArray( platform ) ) {
            platforms = [ platform ]
        } else {
            platforms = Object.keys( Platforms )
        }

        for (let platform of platforms ) {
            console.log(`connecting platform ${platform}`)
            let promise = Platforms[platform].connect(useDefault)

            dispatch( platformConnection( platform, CONNECTING, useDefault ) )

            promise.then( (res) => {
                scAuth.oauth_token = res.oauth_token
                dispatch( platformConnection( platform, READY, useDefault ) )
            })

            promise.catch( err => {
                console.log(`platform error with ${platform}`)
                console.log(err)

                dispatch( platformConnection( platform, ERROR, useDefault ) )
            })
        }
    }
}

export const itemFavorite = (domain, ref) => {
    return (dispatch, getState) => {
        let item
        let state = getState();

        if (state[domain]) {
            item = state[domain][ref]
            
            if (item) {
                let platform = item.platform

                Platforms[platform].favorite(domain, item.originId)

                .then( response => {

                    if (response) {
                        dispatch({
                            type: API_FAVORITE_RESPONSE,
                            domain,
                            ref
                        })
                    }
                })
            }

            dispatch({
                type: API_FAVORITE_REQUEST,
                domain,
                ref
            })
        }

    }
}

const platformConnection = (platform, status, useDefault, err) => {
    return {
        type : API_CONNECTION,
        status,
        platform,
        err,
        meta : {
            category: 'api_connect',
            label: `${platform} ${useDefault ? 'default' : 'user'}`,
            action: `${status}`
        }
    }
}
