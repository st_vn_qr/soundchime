import { 
    TAG_NEW,
    TRACK_TAG_ADD,
    TRACK_TAG_REMOVE,
    CHANNEL_TAG_ADD,
    CHANNEL_TAG_REMOVE,
    ARTIST_TAG_ADD,
    ARTIST_TAG_REMOVE
} from "./index"

export const tagNew = (tagRef) => {
    return {
        type: TAG_NEW,
        tagRef
    }
}

export const channelTagAdd = (channelRef, tagRef) => {
    return {
        type: CHANNEL_TAG_ADD,
        channelRef,
        tagRef
    }
}

export const channelTagRemove = (channelRef, tagRef) => {
    return {
        type: CHANNEL_TAG_REMOVE,
        channelRef,
        tagRef
    }
}
export const trackTagAdd = (trackRef, tagRef) => {
    return {
        type: TRACK_TAG_ADD,
        trackRef,
        tagRef
    }
}

export const trackTagRemove = (trackRef, tagRef) => {
    return {
        type: TRACK_TAG_REMOVE,
        trackRef,
        tagRef
    }
}

export const artistTagAdd = (artistRef, tagRef) => {
    return {
        type: ARTIST_TAG_ADD,
        artistRef,
        tagRef
    }
}

export const artistTagRemove = (artistRef, tagRef) => {
    return {
        type: ARTIST_TAG_REMOVE,
        artistRef,
        tagRef
    }
}
