import { CHANNEL_NEW, CHANNEL_SELECT } from "./index"

export const channelNew = (channelRef, tagRefs, trackListRefs) => {
    return {
        type: CHANNEL_NEW,
        channelRef,
        tagRefs,
        trackListRefs
    }
}

export const channelSelect = channelRef => {
    return {
        type: CHANNEL_SELECT,
        channelRef
    }
}
