import { 
    TRACK_PLAYED, 
    TRACK_PLAY,
    TRACK_SKIP
} from "./index"

export const trackPlayed = (trackRef) => { 
    return {
        type: TRACK_PLAYED, trackRef 
    }
}

export const trackPlay = (trackRef, listName, index) => { 
    return {
        type: TRACK_PLAY, 
        trackRef,
        listName,
        index
    }
}

export const trackSkip = () => {
    return {
        type: TRACK_SKIP
    }
}
