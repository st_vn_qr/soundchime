import React, { Component } from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import { Navigator } from "../components"

import { 
    Tags, 
    Player 
} from "../containers"

import { 
    channelTagAdd,
    channelTagRemove,
    trackTagAdd,
    trackTagRemove,
    artistTagAdd,
    artistTagRemove
} from "../actions/tags"

import { trackPlay } from "../actions/player"

import { channelSelect } from "../actions/channels"


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ 
                channelTagAdd,
                channelTagRemove,
                trackTagAdd,
                trackTagRemove,
                artistTagAdd,
                artistTagRemove,
                channelSelect,
                trackPlay
        }, dispatch)
}


class Tchime extends Component {
    constructor(props) {
        super(props);
        console.log('tchime constructor')
        this.state = {
            menu : {
                closed: true,
                selected : null
            },
            navigation: {
                tracks : "feed",
                artists: "feed"
            },
            scroll : {}
        }

        this.selectListener = {
            listeners : [],
            add : listener => { this.selectListener.listeners.push(listener) },
            remove: listener => this.selectListener.listeners.splice(
            this.selectListener.listeners.indexOf(listener) ),
            call : (selected) => this.selectListener.listeners.forEach( cb => cb(selected) )
        }
    }

    openMenu(ev) {
        ev.stopPropagation();
        this.setState({
            menu : { closed : false, selected: null }
        })
    }

    closeMenu(ev) {
        ev.stopPropagation()

        this.selectListener.call(null)

        if (this.state.menu.closed && this.state.selected === null) {
            return
        } else {
            this.setState({
                menu : { closed : true },
                selected: null
            })

            console.log('closed menu, cleared selection')
        }
    }

    untagItem(selected) {
        switch (selected.parentDomain) {
            case "tracks":
                this.props.trackTagRemove(selected.trackRef, selected.tagRef)
                break
            case "artists":
                this.props.artistTagRemove(selected.artistRef, selected.tagRef)
                break
            case "channels":
                this.props.channelTagRemove(selected.channelRef, selected.tagRef)
                break
        }
    }

    tagItem(selected) {
        switch(selected.domain) {
            case "tracks":
                this.props.trackTagAdd(selected.trackRef,
                                    this.state.selected.tagRef)
                break
            case "artists":
                this.props.artistTagAdd(selected.artistRef,
                                    this.state.selected.tagRef)
                break
            case "channels":
                this.props.channelTagAdd(selected.channelRef,
                                    this.state.selected.tagRef)
                break
        }
    }

    selectItem(selected, ev) {
        if (selected) {
            this.selectListener.call(selected)

            if (this.state.selected) {
                if (this.state.selected.domain === "tagmenu"
                    && selected.domain !== "tagmenu") {

                    maybePreventEvent(ev)
                    this.tagItem( selected )
                    return

                } else if (this.state.selected.domain === "tags" ) {
                    if (selected === this.state.selected) {

                        maybePreventEvent(ev)
                        this.untagItem( selected )

                        console.log(`deleting tag ${selected.tagRef} from ` +
                            `${selected.parentRef} of ${selected.parentDomain}`)
                    }
                } 
            } 

            if (selected.domain === "channels") {
                console.log(`selecting channel ${selected.channelRef}`)
                this.props.channelSelect(selected.channelRef)
            }
        }
        console.log(`selected`)
        console.log(selected)
        maybePreventEvent(ev)

        this.setState({ selected } )
    }

    selectHover(component) {
        if (this.state.selected && this.state.selected.domain === "tagmenu") {
            component.setState(Object.assign({}, component.state, {hover: true}))
        }
    }

    reportScroll(domain, sub, scroll) {

        let scrollObj = this.state.scroll
        let domainObj = this.state.scroll[domain]


        this.setState({
            scroll : Object.assign({}, scrollObj, {
                [domain] : Object.assign({}, domainObj, {
                    [sub] : scroll
                })
            })
        })
    }

    getChildContext() {
        return {
            selected : this.state.selected,
            menu: this.state.menu,
            scroll : this.state.scroll,

            selectItem : this.selectItem.bind(this),
            selectHover : this.selectHover.bind(this),
            selectListener: this.selectListener,
            reportScroll: this.reportScroll.bind(this),

            trackPlay : this.props.trackPlay
        }
    }


    render() {
        console.log('----tchime render')

        return (
            <tchime onClick={ this.closeMenu.bind(this) }>
                { this.props.children }
                <Navigator
                    openMenu={ this.openMenu.bind(this) }
                    location={ this.props.location }
                />
                <Tags 
                    closeMenu={ this.closeMenu.bind(this) }
                    closed={ this.state.menu.closed } 
                />
                <Player />
            </tchime>
        )
    }
}


Tchime.childContextTypes = {
    selected : React.PropTypes.object,
    selectItem : React.PropTypes.func,
    selectHover : React.PropTypes.func,
    selectListener: React.PropTypes.object,
    menu : React.PropTypes.object,
    scroll : React.PropTypes.object,
    reportScroll : React.PropTypes.func,
    trackPlay : React.PropTypes.func
},

Tchime = connect( false, mapDispatchToProps )( Tchime )

export default Tchime 

function maybePreventEvent(ev) {
    if (ev) {
        ev.preventDefault()
        ev.stopPropagation()

        if (ev.nativeEvent) {
            ev.nativeEvent.preventDefault()
            ev.nativeEvent.stopPropagation()
        }
    }
}
