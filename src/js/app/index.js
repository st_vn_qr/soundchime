import ReactDOM from "react-dom"
import "../util/polyfills"

import { 
    Router, 
    Route,
    IndexRoute,
    browserHistory
} from "react-router"

import { Provider } from "react-redux"

import { 
    createStore, 
    applyMiddleware,
    compose
} from "redux"

import thunk from "redux-thunk"

import persistState from "redux-localstorage"


import Tchime from "tchime"
import bgEffects from "../bgEffects"

import { 
    Tracks,
    Artists,
    Channels,
    Info,
    Home
} from "../containers"

import reducers from "../reducers"

import analytics from "../middleware/analytics"

const currentVer = "0.055"
console.log(`SoundChi.me v${currentVer}`)


let storeVersion = window.localStorage.getItem("tchimeVersion")

if (storeVersion !== currentVer) {
    window.localStorage.removeItem("tchime")
    window.localStorage.removeItem("tchime-version")
    window.localStorage.removeItem("tchimeVersion")
}


let middleWare = [ thunk ]

if (window.ga) {
    middleWare.push(analytics)

    browserHistory.listen(function(location) {
        window.ga('send', 'pageview', location.pathname)
    })
}


let enhancers = compose(
    applyMiddleware( ...middleWare ),
    persistState(["tracks","artists","channels","tags"], { key: 'tchime' }),
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

let store = createStore( reducers, enhancers );

window.store = store;

ReactDOM.render( 
    <Provider store={ store }>

        <Router history={ browserHistory }>

            <Route path="/" component={ Tchime } >

            <Route path="/tracks" component={ Tracks }>
            <Route path=":list" component={ Tracks }/>
            </Route>

            <Route path="/artists" component={ Artists }>
            <Route path=":list" component={ Artists } />
            </Route>

            <Route path="/channels" component={ Channels } />

            <Route path="/info/:domain/:ref" component={ Info } />

            <IndexRoute component={ Home } />

            </Route>


        </Router>

    </Provider>
    ,document.querySelector( "app-fg" )
);

bgEffects.start();


window.localStorage.setItem("tchimeVersion", currentVer)
