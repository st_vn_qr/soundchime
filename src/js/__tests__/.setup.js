"use strict";
import { jsdom } from 'jsdom'
import mockery from 'mockery'
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'

import module from 'module'

(function (){
    var origEnv = process.env.NODE_ENV

    process.env.NODE_ENV = 'test'

    process.on('exit', function() {
        process.env.NODE_ENV = origEnv
    })
})()

var origLoader = module._load

module._load = function(request, parent, isMain) {
    try {
        return origLoader(request, parent, isMain)
    } catch (e) {
        console.log(`Failed to load ${request}\n\t\tfrom ${parent.filename}`)
        throw e
    }
}


global.should = chai.should()
chai.use(chaiAsPromised)


var exposedProps = ['window', 'navigator', 'document']

global.document = jsdom('')
global.window = document.defaultView
Object.keys(document.defaultView).forEach( property => {
    if (typeof global[property] === 'undefined') {
        exposedProps.push(property)
        global[property] = document.defaultView[property]
    }
})

global.navigator = { userAgent: 'node.js' }


// mock bgEffects
// needs to use require to avoid import hoisting

var bg = require('./.mock_bgEffects.js')

mockery.registerMock('../bgEffects', bg )
mockery.enable({ warnOnUnregistered: false})
