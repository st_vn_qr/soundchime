/* eslint-env node, mocha */
/* global should */
'use strict';

import React from 'react'
import { shallow, mount } from 'enzyme'
import { configureStore } from "redux-mock-store"
 

import Artist from '../components/Artist'
import Tchime from '../app/tchime'

const mock_artist = {
    ref : 'mock_artist_001',
    name : 'mock_star'
}

const mock_tchime_context = {
    selectItem: function() {},
    selected: null
}

//const store = configureStore()

describe('Tchime.jsx', () => {
    it('renders a <tchime> element', () => {
        const wrapper = shallow(<Tchime />)
        
        let mainEl = wrapper.find('tchime').first()

        should.exist(mainEl)
    })
})

describe('Artist.jsx', () => {
    it('renders an artist element', () => {
        const wrapper = mount(<Artist item={mock_artist} />,
                              { 
                                  context: mock_tchime_context,
                                  childContextTypes : {
                                      selectItem: React.PropTypes.func,
                                      selected: React.PropTypes.object
                                  } 
                              } 
                             )

        let mainDiv = wrapper.find('div').first()
        should.exist(mainDiv)
        mainDiv.hasClass('artist').should.be.true
    })
})

