/* eslint-env mocha */

console.log('running test')
import chai from 'chai'
import reducers from '../reducers'

import { resourceRetrieved } from '../actions/api'

import {
    tagNew,
    trackTagAdd
} from '../actions/tags'

import {
    channelNew,
    channelSelect
} from '../actions/channels'

import { READY } from '../api/status'
import { 
    TRACK_FEED,
    TRACK_FAVORITES
} from '../api/resources'

const MockPlatform = 'MockPlatform'

var should = chai.should()

describe('reducers', () => {
    const mock_response_feed = {
        tracks: [
            {
                ref: 'track_001',
                artistRef: 'artist_001',
                title: 'mock track'
            },
            {
                ref: 'track_002',
                artistRef: 'artist_001',
                title: 'another track'
            }
        ],
        artists: [
            {
                ref: 'artist_001',
                name: 'mocker'
            }
        ]
    }

    const mock_response_favorites = {
        tracks: [
            {
                ref: 'mock_track_003',
                title: 'really good mock track',
                artistRef: 'mock_artist_002'
            }
        ],
        artists: [
            {
                ref: 'mock_artist_002',
                name: 'mock star'
            }
        ]
    }

    const mock_response_total_unique_artists = 2
    const mock_response_total_unique_tracks = 3

    const mock_track_1 = mock_response_feed.tracks[0]

    const initialState = reducers(undefined, {})

    describe('empty action', () => {
        it('should have initial state shape', () => {
            initialState.should.have.keys(
                'api','artists','tracks','lists','player','channels','tags'
            )
        })
    })

    const feedState = reducers(initialState, resourceRetrieved(
        TRACK_FEED, MockPlatform, mock_response_feed
    ))

    const favoritesState = reducers(feedState, resourceRetrieved(
        TRACK_FAVORITES, MockPlatform, mock_response_favorites
    ))

    describe('resourceRetrieved', () => {
        describe('TRACK_FEED', () => {
            describe('api', () => {
                it('should set platform status to ready', () => {
                    feedState.should.have.property('api')

                    feedState.api
                        .should.have.property(MockPlatform)

                    feedState.api[MockPlatform].status
                        .should.equal(READY)
                })
            })

            describe('tracks', () => {
                it('should add tracks from response', () => {
                    feedState.should.have.property('tracks')
                    Object.keys(feedState.tracks).should.have.length(
                        mock_response_feed.tracks.length)

                    let track = feedState.tracks[mock_track_1.ref]

                    should.exist(track)
                    track.ref.should.equal(mock_track_1.ref)
                    track.should.contain.all.keys(['ref','artistRef','title'])
                })
            })

            describe('artists', () => {
                it('should add an artist', () => {
                    feedState.should.have.property('artists')
                    Object.keys(feedState.artists).should.have.length(1)
                    feedState.artists.should.have
                        .property(mock_response_feed.artists[0].ref)
                })

                it('should add trackRefs', () => {
                    let artist = feedState.artists[mock_response_feed.artists[0].ref]

                    should.exist(artist)
                    artist.should.have.property('trackRefs')

                    artist.trackRefs
                        .should.contain(mock_response_feed.tracks[0].ref)

                    artist.trackRefs.length
                        .should.equal(mock_response_feed.tracks.length)
                })
            })

            describe('lists', () => {
                it('should add tracks to "feed"', () => {
                    feedState
                        .should.contain.key('lists')

                    feedState.lists
                        .should.contain.key('tracks')

                    feedState.lists.tracks
                        .should.contain.key('feed')

                    feedState.lists.tracks["feed"]
                        .should.have.length( mock_response_feed.tracks.length )
                })
            })
        })


        describe('TRACK_FAVORITES', () => {
            describe('tracks', () => {
                it('should add tracks from response', () => {
                    favoritesState.tracks.should.contain.key(
                        mock_response_favorites.tracks[0].ref
                    )

                    Object.keys( favoritesState.tracks )
                        .should.have.length( mock_response_total_unique_tracks )
                })
            })

            describe('artists', () => {
                it('should add artists', () => {
                    favoritesState.artists
                        .should.contain.key( mock_response_favorites.artists[0].ref )
                    Object.keys( favoritesState.artists )
                        .should.have.length( mock_response_total_unique_artists )
                })
            })

            describe('lists', () => {
                it('should add tracks to "favorites"', () => {
                    favoritesState.lists
                        .should.contain.key('tracks')

                    favoritesState.lists.tracks
                        .should.contain.key('favorites')

                    favoritesState.lists.tracks["favorites"]
                        .should.have.length( mock_response_favorites.tracks.length )
                })
            })
        })
    })

    const mock_tag_1 = 'mock_tag_001'
    const mock_tag_2 = 'mock_tag_002'

    const taggedNextState = reducers(favoritesState, tagNew(mock_tag_1))

    describe('tagNew', () => {
        it('should add a new tag', () => {
            taggedNextState.should.have.property('tags')
            taggedNextState.tags.should.have.length(1)
            taggedNextState.tags[0].should.have.property('ref', mock_tag_1)
        })
    })

    const trackTaggedState1 = reducers(taggedNextState, trackTagAdd(
        mock_track_1.ref, mock_tag_1))

    const trackTaggedState2 = reducers(trackTaggedState1, trackTagAdd(
        mock_track_1.ref, mock_tag_2))

    const trackTaggedState = trackTaggedState2

    describe('trackTagAdd', () => {
        describe('add a tag', () => {
            it('should add tag to track.tagRefs', () => {
                let track = trackTaggedState1.tracks[mock_track_1.ref]

                should.exist(track)

                track.should.have.property('tagRefs')
                track.tagRefs.should.have.length(1)
                track.tagRefs[0].should.be.equal(mock_tag_1)
            })
        })

        describe('add another tag', () => {
            it('should add the second tag', () => {
                let track = trackTaggedState2.tracks[mock_track_1.ref]

                track.tagRefs.should.have.length(2)
                track.tagRefs[1].should.be.equal(mock_tag_2)
            })
        })
    })

    const mock_channel_1 = {
        ref: 'mock_channel_001'
    }

    const mock_channel_2 = {
        ref: 'mock_channel_002',
        tagRefs: mock_tag_1
    }

    const mock_channel_3 = {
        ref: 'mock_channel_003',
        tagRefs: mock_tag_1,
        trackListRefs : ['favorites']
    }

    var channelNextState = reducers( trackTaggedState, channelNew(
        mock_channel_1.ref, mock_channel_1.tagRefs, 
        mock_channel_1.trackListRefs) )

    describe('channelNew', () => {
        describe('empty channel', () => {
            it('should have all the props of a channel', () => {
                channelNextState.should.have.property('channels')
                let channel = channelNextState.channels[mock_channel_1.ref]

                should.exist(channel)

                channel.should.contain.keys('tagRefs', 'ref','trackListRefs')
            })
        })

        channelNextState = reducers( channelNextState, channelNew(
            mock_channel_2.ref, mock_channel_2.tagRefs, 
            mock_channel_2.trackListRefs) )

        describe('tagged channel', () => {
            it('should have correct tags', () => {
                let channel = channelNextState.channels[mock_channel_2.ref]

                channel.should.contain.keys('tagRefs', 'ref','trackListRefs')
                channel.tagRefs.should.be.an('array')
                channel.tagRefs.should.have.length(1)
            })
        })

        channelNextState = reducers( channelNextState, channelNew(
            mock_channel_3.ref, mock_channel_3.tagRefs, 
            mock_channel_3.trackListRefs) )

        describe('tagged channel, favorite only', () => {
            it('should only have favorites in trackListRefs', () => {
                let channel = channelNextState.channels[mock_channel_3.ref]

                channel.should.contain.keys('tagRefs', 'ref','trackListRefs')
                channel.trackListRefs.should.be.an('array')
                channel.trackListRefs.should.contain('favorites')
            })
        })
    })

    var channelSelectedState = reducers( channelNextState, channelSelect(
        mock_channel_1.ref) )

    describe('channelSelect empty channel', () => {
        let playerState = channelSelectedState.player

        it('should set player properties', () => {
            playerState.should.contain.keys(
                'currentTrackRef','nextTrackRef', 'pastTrackRefs', 
                'currentChannelRef', 'nextIndex' )

        })

        it('should set first track available as currentTrack', () => {
            playerState.currentTrackRef.should.equal( mock_track_1.ref )
        })
    })

    channelSelectedState = reducers( channelNextState, channelSelect(
        mock_channel_1.ref) )

    describe('channelSelect tagged channel', () => {
    })
})
