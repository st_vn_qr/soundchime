import bgEffects from '../bgEffects'

console.log('mocking bgEffects')

function replaceFuncsWithNoop(obj) {
    let res = {}

    for (let prop in obj) {
        if (typeof obj[prop] === 'function') {
            res[prop] = noop
        } else if (typeof obj[prop] === 'object') {
            res[prop] = replaceFuncsWithNoop(obj[prop])
        }
    }
    
    return res
}

function noop() {}

var bg = replaceFuncsWithNoop(bgEffects)

export default bg
