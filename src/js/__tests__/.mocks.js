export const mock_track = {
    ref: 'mock_track_001',
    artistRef: 'mock_artist_001',
    title: 'mock track'
}

export const mock_tracks = [
    mock_track,

    {
        ref: 'mock_track_002',
        artistRef: 'mock_artist_001',
        title: 'mock track 2'
    },
    {
        ref: 'mock_track_003',
        artistRef: 'mock_artist_001',
        title: 'mock track 3'
    },
    {
        ref: 'mock_track_004',
        artistRef: 'mock_artist_002',
        title: 'mock track 4'
    },
    {
        ref: 'mock_track_005',
        artistRef: 'mock_artist_002',
        title: 'mock track 5'
    },
]
