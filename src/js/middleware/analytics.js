/* global ga */

const analytics = () => next => action => {
    if (action.meta) {
        ga('send', {
            hitType: 'event',
            eventCategory: action.meta.category,
            eventAction: action.meta.action,
            eventLabel: action.meta.label
        })
    } else {
        ga('send', {
            hitType: 'event',
            eventCategory: 'action',
            eventAction: action.type,
            eventLabel: action.ref || action.trackRef ||
                action.channelRef || action.tagRef || undefined
        })
    }

    return next(action)
}

export default analytics
