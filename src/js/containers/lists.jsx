/* eslint no-debugger:0 */

import React from "react"
import { List } from "../components"

function Lists(props) {
    if (!props.params.list) {

        console.log(`nothing at ${props.location.pathname}`);

        return (<div>Nothing here! (Check the URL)</div>);
    }

    return ( <List {...props } />)
}

Lists.propTypes = {
    params : React.PropTypes.object.isRequired,
    location: React.PropTypes.object.isRequired,
    itemComponent: React.PropTypes.func.isRequired,
    domain: React.PropTypes.string.isRequired,
    itemsFromState : React.PropTypes.shape({
        originalItems: React.PropTypes.array,
        processedItems: React.PropTypes.array,
        listenObject : React.PropTypes.object
    })
}

export default Lists
