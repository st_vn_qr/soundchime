import { connect } from "react-redux"

import { TagMenu } from "../components"
import { tagNew } from "../actions/tags"
import { 
    tagTotalCountSelector 
} from '../selectors'

function mapDispatchToProps(dispatch) {
    return {
        addTag : (tagRef) => {
            dispatch( tagNew( tagRef ) );
        }
    }
}
function mapStateToProps(state) {
    return {
        tagCounts : tagTotalCountSelector(state)
    }
}

function Tags(props) {
    return (
        <TagMenu {...props} />
    )
}

Tags = connect( mapStateToProps, mapDispatchToProps )( Tags );

export default Tags
