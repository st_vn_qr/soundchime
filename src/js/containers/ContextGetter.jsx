import React from "react"


function ContextGetter(props, context) {
    return (
        <div style={
            {
                position: "fixed",
                bottom: "0px"
            }
        }>Context: { context.selected && 
            context.selected.ref }</div>
    )
}



ContextGetter.contextTypes = {
    selected : React.PropTypes.object,
    selectItem : React.PropTypes.func
}


export default ContextGetter
