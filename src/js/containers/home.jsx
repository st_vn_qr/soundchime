import { Link } from "react-router"

import { connectPlatform } from "../actions/api"

import { connect } from "react-redux"

import { READY, RETRIEVING } from "../api/status"
import { SoundCloud } from "../api/platforms"

function PastTracks(props) {
    return (
        <div className="past">
            Past Tracks:
            {
            props.tracks
            .map( track => {
                    return (
                        <div key={track.ref}>
                            <Link to={ `/info/track/${track.ref}`}
                                >{track.artist}-{track.title}</Link>
                        </div>
                    )
                })
        }</div>
    )
}

function Intro() {
    return (
        <div>
            <br />
            <p>Open the menu at the top right to add or select a tag - then click on any track, artist or channel to tag it.</p>
            <br />
            <br />
            <p>The tracks tab has lists of tracks from your feed and favorites.</p>
            <br />
            <p>The artists tab shows who's popping up in those track lists.</p>
            <br />
            <p>Use the channels tab to create new channels or to choose one to listen to. Channels play tracks from both feed and favorites, or you can limit it to just favorites. Be sure to open the tracks tab to load up some tracks -- channels only play from what's there.</p>
            <br />
            <br />
            <br />
            <p>This place is still under construction, but most of the core functionality is available. Let me know if you have any feedback!</p>
            <a href="mailto:hi@steven.solar">(email)</a>
            <br />
            <br />
            <p>(You'll need to connect a SoundCloud account)</p>
            <br />
        </div>
    )
}

function Home(props) {
    return (
        <div className="home tab">{ 
            props.pastTracks.length ? 
                <PastTracks tracks={ props.pastTracks } />
                    : <Intro /> }
            <button onClick={ props.connect.bind(null, false) }>
                <span>{ props.connected[SoundCloud] ? "connected to" 
                        :"connect to" }</span>
                <img src="/static/images/sc_logo_text.png" />
            </button>
            <br />
            <p style={ {textAlign : "center" }}>or</p>
            <br />
            <button onClick={ props.connect.bind(null, true) }>
                <span> use mine </span>
            </button>
        </div>
    )
}

Home = connect( 
               state => { 
                   let pastTrackRefs = state.player.pastTrackRefs
                   let tracks = state.tracks

                   let pastTracks = pastTrackRefs.map( ref => tracks[ref] )

                   let connected = Object.keys(state.api).reduce( (res, api) => {
                       return res[api] = { 
                           [api] : 
                               ( state.api[api].status === READY ||
                               state.api[api].status === RETRIEVING ) }
                   }, {})

                   return { pastTracks, connected }
               }, 
               dispatch => {
                   return { 
                       connect : (useDefault) => { 
                           dispatch( connectPlatform(useDefault) ); } 
                   }
               })( Home );

export default Home
