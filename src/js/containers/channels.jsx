import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import { ChannelCreate, ChannelList } from "../components"
import { channelNew } from "../actions/channels" 
import { trackPlayed } from "../actions/player"



function stateProps (state) {
    return {
        tags : state.tags,
        channels : state.channels
    }
}

function channelActions (dispatch) {
    return bindActionCreators({
        channelNew,
        trackPlayed
    }, dispatch)
}


function Channels(props) {
    console.log('channel render')
    return (
        <div className="tab">
            <ChannelCreate create={ props.channelNew } />
            <ChannelList channels= { props.channels } />
        </div>
    )
}


Channels = connect( stateProps, channelActions )( Channels );

export default Channels
