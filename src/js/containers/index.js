import Tracks from "./tracks"
import Artists from "./artists"
import Channels from "./channels"
import Info from "./info"
import Tags from "./tags"
import Player from "./player"
import Home from "./home"


export { 
    Tracks, 
    Artists,
    Channels,
    Info,
    Tags,
    Player,
    Home
};
