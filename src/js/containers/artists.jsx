import { connect } from "react-redux"
import { Artist } from "../components"
import Lists from "./lists"

function mapState(state, props) {
    let list, listName

    let items = []

    listName = props.params.list

    
    if (state.lists && state.lists[props.domain]) {
        list = state.lists[props.domain][listName]
    }

    if (list) {

        let len = list.length
        let objItems = {}

        for (let i=0; i<len; i++) {
            let item = state[ props.domain ][ list[i] ]

            if (item) {

                if (objItems[item.ref]) {
                    objItems[item.ref].count += 1
                } else {
                    objItems[item.ref] = Object.assign({}, item, {
                        count : 1
                    })
                }
            }
        }

        for (let ref in objItems) {
            items.push( objItems[ref] )
        }
    }

    return { 
        itemsFromState: { 
            processedItems: items,
            originalItems: list,
            listenObject: state.artists 
        }
    }
}

const Artists = connect( mapState ) ( Lists )

export default function ArtistsWrapper(props) {
    return(
        <Artists 
            params={ props.params }
            location={ props.location }
            itemComponent={ Artist }
            domain="artists" 
            listNames={ ["feed","favorites"] }
        />
    )
}
