import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { PlayerDashboard } from "../components"

import { 
    trackPlayed, 
    trackSkip,
    trackPlay
} from "../actions/player"


function mapState(state) {
    let pastTrackRefs, pastTracks, track, nextTrack, player, channel

    player = state.player

    track = state.tracks[ player.currentTrackRef ]
    nextTrack = state.tracks[ player.nextTrackRef ]
    channel = state.channels[ player.currentChannelRef ]

    pastTrackRefs = state.player.pastTrackRefs.slice(-2)
    if (pastTrackRefs) pastTracks = pastTrackRefs.map( ref => state.tracks[ref] )

    return { player, track, nextTrack, channel, pastTracks }
}

function mapDispatch(dispatch) {
    return bindActionCreators({
        trackPlayed,
        trackSkip,
        trackPlay
    }, dispatch)
}

function Player(props) {
    return (
        <PlayerDashboard {...props} />
    )
}

Player = connect( mapState, mapDispatch )( Player )

export default Player
