import { connect } from "react-redux"
import { bindActionCreators } from "redux"

import { Track } from "../components"
import Lists from "./lists"

import { retrieveResource } from "../actions/api"
import { SoundCloud } from "../api/Platforms"
import { 
    TRACK_FEED,
    TRACK_FAVORITES
} from "../api/resources"


const mapDispatch = dispatch => {
    return bindActionCreators({
        retrieveResource
    }, dispatch)
}

function mapStateToProps(state, ownProps) {
    let listName, list;
    let items = [];

    listName = ownProps.params.list;

    if (state.lists && state.lists.tracks) list = state.lists.tracks[listName];

    let tracks = state.tracks;

    if (list) {

        let len = list.length;

        for (let i=0;i<len;i++) {

            let item_ = tracks[ list[i] ];

            if (item_) {

                let item = Object.assign({}, item_, {
                    artist : state.artists[ item_.artistRef ].name 
                });

                items.push(item);
            }

        }
    }

    return { 
        itemsFromState: { 
            processedItems: items,
            originalItems: list,
            listenObject : tracks
        } 
    };
}

const Tracks = connect(mapStateToProps, mapDispatch)( Lists )

function TracksWrapper(props) {
    return (
        <Tracks
            itemComponent={ Track }
            domain="tracks"
            itemsFromState={ props.itemsFromState }
            params={ props.params }
            location={ props.location }
            requestList={ 
                {
                    favorites : { 
                        type : TRACK_FAVORITES,
                        platform : SoundCloud 
                    },
                    feed : {
                        type : TRACK_FEED, 
                        platform: SoundCloud
                    }
                }
            }
        />
    )
}

export default TracksWrapper
