import React from "react"

import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import { 
    TrackInfo,
    ArtistInfo,
    PoweredBySoundCloud
} from "../components"

const mapDispatch = dispatch => bindActionCreators()

function stateProps(state, props) {
    if (props.params.domain === "track") {

        let track = state.tracks[props.params.ref];

        return { 
            artist : state.artists[ state.tracks[props.params.ref].artistRef ],
            track
        }

    } else if (props.params.domain === "artist") {

        let artist = state.artists[props.params.ref]
        let trackRefs = artist.trackRefs

        let lists = {} 

        for (let listKey in state.lists.tracks) {
            let stateList = state.lists.tracks[ listKey ]

            lists[listKey] = [];

            for (let i=0; i<stateList.length; i++) {
                if (trackRefs.indexOf( stateList[i] ) !== -1) {
                    let track = state.tracks[ stateList[i] ]

                    lists[listKey].push( track )
                }
            }

        }
        return { 
            artist : state.artists[props.params.ref], 
            lists
        } 
    }
}

function Info(props) {
    let item

    switch (props.params.domain) {

        case "track":

            item = <TrackInfo track={ props.track } artist={ props.artist } />

            break

        case "artist" :

            item = <ArtistInfo {...props} />

            break
    }

    return (
        <div className="tab">{ item || "Nothing here" }
            <PoweredBySoundCloud />
        </div>
    )
}

Info.propTypes = {
    params : React.PropTypes.object.isRequired
}

Info = connect( stateProps )( Info )

export default Info
