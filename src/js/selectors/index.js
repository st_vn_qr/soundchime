import { createSelector } from 'reselect'

const getTrackTagCounts = (state) => {
    return Object.keys( state.tracks ).reduce( (tagCounts, trackRef) => {
        let track = state.tracks[trackRef]
        if (track.tagRefs) {
            for (let tagRef of track.tagRefs) {
                tagCounts[tagRef] = (tagCounts[tagRef] + 1) || 1
            }

        }

        return tagCounts
    }, {})
}

const getArtistTagCounts = state => {
    return Object.keys( state.artists ).reduce( (tagCounts, artistRef) => {
        let artist = state.artists[artistRef]

        if (artist.tagRefs) {
            for (let tagRef of artist.tagRefs) {
                tagCounts[tagRef] = (tagCounts[tagRef] + 1) || 1
            }

        }

        return tagCounts
    }, {})
}

export const tagCountSelector = createSelector(
    getTrackTagCounts,
    trackTagCounts => trackTagCounts
)

export const artistTagCountSelector = createSelector(
    getArtistTagCounts,
    artistTagCounts => artistTagCounts
)

export const tagTotalCountSelector = createSelector(
    getTrackTagCounts, getArtistTagCounts,
    (trackTagCounts, artistTagCounts) => {

        let tagRefs = Object.keys( Object.assign({}, trackTagCounts, artistTagCounts) )

        tagRefs.sort( (tagB, tagA) => {
            let res = ( ( trackTagCounts[tagA] | 0 ) + ( artistTagCounts[tagA] ) | 0 ) - 
                ( ( trackTagCounts[tagB] | 0 ) + ( artistTagCounts[tagB] | 0 ) )
            return res
        })

        return tagRefs.reduce( (tags, tagRef) => {
            tags[tagRef] = {
                artistCount : artistTagCounts[tagRef] | 0,
                trackCount : trackTagCounts[tagRef] | 0
            }

            return tags
        }, {})
    }
)
