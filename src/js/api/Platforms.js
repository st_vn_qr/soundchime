/*eslint-disable*/
export const SoundCloud = "SoundCloud";

import { 
    TRACK_FEED, 
    TRACK_FAVORITES,
    ARTIST_INFO
} from "./resources";

import { MultiPromise } from "../util/PromiseUtils"

import scAuth from "../api/scAuth"

let Platforms = {};

let Parsers = {
    [SoundCloud] : {
        parseArtistFromTrack : function( track ) {
            return {
                permalink : track.user.permalink_url,
                name : track.user.username,
                originId : track.user.id,
                ref : `${SoundCloud}:${track.user.id}`,
                imageURL : track.user.avatar_url
            }
        },

        parseTrack : function( track ) {
            return {
                platform : SoundCloud,
                ref : `${SoundCloud}:${track.user.id}:${track.id}`,
                artistRef : `${SoundCloud}:${track.user.id}`,
                artist: track.user.username,
                title : track.title,
                streamURL : track.stream_url,
                originId : track.id,
                permalink : track.permalink_url,
                favorite : track.user_favorite,
                imageURL : track.artwork_url,
                duration : track.duration,
                description : track.description
            }
        },

        parseArtist : function( artist ) {
        }
    }
}

function pageSoundCloudTracks(list) {
    let cursor, target, getTracks
    let parser = Parsers[SoundCloud]


    let options = {
        limit: 20,
        linked_partitioning: 1,
        format:"json"
    }


    let reg = /cursor=([a-z0-9-]+)/
    

    let getCursor = response => {
        if (response.next_href) {

            let regMatch = reg.exec(response.next_href)

            if (regMatch) return regMatch[1]
        } 

        return false
    }

    if (list === "feed") {
        target = "me/activities"

        getTracks = response => {
            return response.collection.filter(
                o => o.type && o.type.startsWith("track") && o.origin
            ).map(
                t => t.origin
            )
        }

    } else if (list === "favorites") {
        target = "me/favorites"

        getTracks = response => response.collection
    }

    return function() {
        console.log(`requesting ${list} from SoundCloud`)

        if (cursor === false ) {
            return new Promise( resolve => resolve({end : true }) )
        } else {

            let url = scAuth.getTargetURL(target, options)

            console.log(`fetching from ${url}`)

            return fetch(url).then( response => response.json() )
            .then( response => {
                console.log(response)

                let tracks = getTracks( response )
                options.cursor = getCursor(response)

                console.log(`>> tracks >>`)
                console.log(tracks)

                return ({
                    artists: tracks.map( parser.parseArtistFromTrack ),
                    tracks:  tracks.map( parser.parseTrack )
                })
            })
        }
    }
}

function simpleRequest(target) {
    let parser = Parsers[SoundCloud]

    return function(specifier) {
        if (!specifier) return new Promise( (res, rej) => rej("No specifier") )
        let targetURL = scAuth.getPublicTargetURL(specifier)

        return fetch(targetURL).then( response => response.json() )
    }
}

Platforms[SoundCloud] = {
    retrieve : {
        [TRACK_FEED] : pageSoundCloudTracks("feed"),
        [TRACK_FAVORITES] : pageSoundCloudTracks("favorites"),
        [ARTIST_INFO] : simpleRequest("users/")
    },

    connect : function(useDefault) {
        if (useDefault === true) {

            return new Promise( resolve => resolve(true) )
        } else {

            return scAuth.connect();
        } 
    },

    favorite : function(domain, id) {
        let endpoint 

        if (domain === "tracks") endpoint = "me/favorites/"
        else if (domain === "artists") endpoint = "me/followings/"

        if (endpoint) return window.SC.put(endpoint + id)
    }
}


// Methods on Platforms route to all platforms
// Only platforms are enumerable on Platforms

function retrieveFromEveryPlatform( Platforms, resource ) {
    let promises = [];

    for (let plat in Platforms) {

        let promise = Platforms[plat].retrieve[resource]();

        promises.push( promise );
    }

    return new MultiPromise( promises );
}


[ TRACK_FEED, TRACK_FAVORITES, ARTIST_INFO ]
.forEach( resource => {
    Object.defineProperty( Platforms, resource,
        { 
            value : {
                retrieve : retrieveFromEveryPlatform
                    .bind( null, Platforms, resource )
            }
        }
    );
}); 

Object.defineProperty( Platforms, "connect", {
    value : () => {

        let promises = [];

        for (let plat in Platforms) {

            let promise = Platforms[plat].connect();

            promises.push( promise );
        }

        return new MultiPromise( promises );
    }
});

export default Platforms;
