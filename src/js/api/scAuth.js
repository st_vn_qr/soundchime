let defaultOauth = "__default_token_goes_here__"

const tokenRe = /access_token=([^&]+)/

const SCAuth = function() { 
    let oauth_token  = defaultOauth

    let baseURL = "https://api.soundcloud.com/"
    let client_id = "__client_id_goes_here__"
    let redirect_uri = "__redirect_uri_goes_here__"

    let resolveConnect = null
    
    return {
        connect : function() {
            window.open(`https://soundcloud.com/connect?` +
                        `client_id=${client_id}&` +
                        `redirect_uri=${redirect_uri}&` +
                        `display=popup&` +
                        `response_type=code_and_token&` +
                        `scope=non-expiring&` +
                        `state=SoundCloud_Dialog_19f94`,'Connect to SoundCloud',
                        "location=1, width=456, height=510, toolbar=no, " +
                        "scrollbars=yes, " + 
                        `left=${window.screen.width / 2 - 228}, ` +
                        `top=${window.screen.height / 2 - 255}`
                       )

            return new Promise( resolve => resolveConnect = resolve )
        },

        connectCallback : function() {

            console.log('connect callback')
            let match = tokenRe.exec(this.location.href)
            if (match) {
                match = match[1]

                if (match) {
                    console.log('got oauth')
                    oauth_token = match
                    console.log(oauth_token)
                } else {
                    console.log('auth failed')
                }

            }

            if (resolveConnect) {
                console.log('resolving')
                resolveConnect(true)
            }

            this.close()

        },

        getTargetURL : function(target, options=null) {
            return buildQueryURL(baseURL + target, 
                                 Object.assign({ oauth_token, client_id }, 
                                                   options) )
        },

        getPublicTargetURL : function(target, options=null) {
            return buildQueryURL(baseURL + target, 
                                 Object.assign({ client_id }, 
                                                   options) )
        },

        getStreamURL : function(trackStreamURL) {
            let clientOptions = {
                client_id,
                oauth_token
            }

            let queryString = Object.keys(clientOptions)
                .map( key => `${key}=${clientOptions[key]}`).join("&")

            return `${trackStreamURL}?${queryString}`
        }
    }
}

const scAuth = SCAuth()


function buildQueryURL(url, params) {
    if (!params) return url
    return url + "?" + Object.keys(params).map( key => `${key}=${params[key]}`).join("&")
}

window.connectCallback = scAuth.connectCallback
export default scAuth
