function bgEffects() {
    // DOM pointers
    let logo = {}
    let chimes = {}
    let clouds = {}

    // Image loaded callbacker
    let images = {
        loading : [],
        allLoadedListeners: []
    }

    let imageIDCounter = 0;

    let timers = {}


    // Background animate color dictionary
    let colors = {
        dim: "#5b5b5b",
        red: "#f33",
        yellow: "#ff8",
        green: "#5c5"
    }

    colors.default = colors.dim;


    return {
        start : function() {

            // Grab DOMNode 's
            // bg

            logo.svg = document.querySelector("body > app-bg > svg");
            logo.color = document.querySelector("#svgColor");
            chimes.group = document.querySelector("#svgChimes");
            clouds.group = document.querySelector("#svgClouds");

            if (chimes.group) chimes.nodes = chimes.group.childNodes;
            if (clouds.group) clouds.nodes = clouds.group.childNodes;

            logo.color.setAttribute("fill", colors.dim);
        },

        animate : function() {
            this.glow();
            this.distort();
        },
        
        stop : function() {
            let cloudsLen = clouds.nodes.length
            let chimesLen = chimes.nodes.length

            for (let timer in timers) {
                clearTimeout(timers[timer])
            }

            for(let i=0; i < cloudsLen; i++) {
                clouds.nodes[i].style.transform = "none"
            }

            for(let i=0; i < chimesLen; i++) {
                chimes.nodes[i].style.opacity = 1
                chimes.nodes[i].style.transform = "none"
                chimes.nodes[i].setAttribute( "height",
                                             i * 5 + 45 )
                chimes.nodes[i].getAttribute( "height")
            }
        },

        distort : function() {
            timers.distort = setTimeout(this.distort.bind(this), 500)

            let ind = Math.floor(Math.random() * clouds.nodes.length )
            let scale = 0.9 + (Math.random()/10)

            clouds.nodes[ind].style.transform = "scale(" + scale + ")"
        },

        glow : function() {
            timers.glow = setTimeout(this.glow.bind(this), 500)

            let ind = Math.floor(Math.random() * chimes.nodes.length )
            let opacity = 0.5 + Math.random() / 2

            chimes.nodes[ind].style.opacity = opacity

            chimes.nodes[ind].style.transform = "scaleY(" + 
                (0.5 + Math.random()) + ")"

        },

        wave : function(chimeNode=0) {
            if (chimeNode >= chimes.nodes.length) chimeNode = 0
            chimes.nodes[chimeNode].setAttribute("height", "30")
            timers[`waveUp${chimeNode}`] = 
                setTimeout( this.wave.bind(this, chimeNode + 1), 200 )
            timers[`waveDown${chimeNode}`] = 
                setTimeout( this.waveDown.bind(this, chimeNode), 500 )

        },

        waveDown: function(chimeNode) {
            chimes.nodes[chimeNode].setAttribute("height", "70")
        },

        flash : function(color) {
            this.color(color)
            this.clear(400)
        },

        color : function(color) {
            setTimeout( () => {
                logo.color.setAttribute("fill", colors[color])
            }, 1)
        },

        clear : function(delay) {
            setTimeout( () => {
                logo.color.setAttribute("fill", colors.default)
            }, delay || 17)
        },

        images : {
            getLoaderID : function() {
                console.log('add image loader')
                images.loading.push( imageIDCounter++ )
                return imageIDCounter
            },

            loaded : function(id) {
                console.log('loaded image')
                let ind = images.loading.indexOf(id)

                if (ind >= 0) {
                    images.loading.splice(ind, 1)
                }

                if (images.loading.length === 0) {
                    this.allLoaded()
                }
            },

            addAllLoadedListener : function(cb) {
                console.log('add image loaded listener')
                images.allLoadedListeners.push(cb)
            },

            allLoaded : function() {
                console.log('all images loaded')
                let cb = images.allLoadedListeners.pop()

                while ( cb ) {
                    cb()
                    cb = images.allLoadedListeners.pop()
                }
            }
        }
    }
}

const bg = bgEffects()
window.bg = bg

export default bg
