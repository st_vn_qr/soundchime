import React, { Component } from "react"

import bgEffects from "../bgEffects"
import scAuth from "../api/scAuth"
import Seeker from "./Seeker"



class AudioPlayer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            paused: false
        }

        this.events = [
            "ended",
            "loadstart", 
            "progress", 
            "canplaythrough", 
            "error", 
            "playing"
        ]

        this.player = new Audio()
        this.buffer = new Audio()

        this.boundEventHandler = this.eventHandler.bind(this)
        this.togglePlay = this._togglePlay.bind(this)
        this.handleKey = this._handleKey.bind(this)
        this.primePlay = this._primePlay.bind(this)
        this.skipTrack = this._skipTrack.bind(this)
        this.seekPosition = this._seekPosition.bind(this)

        this.checkTrackPositionIntvl = null;
    }

    eventHandler(ev) {
        switch (ev.type) {
            case "progress":
                if (ev.target.buffered.length >= 1 && 
                    ev.target.buffered.end(0) === ev.target.duration) {
                    console.log('load finished')
                    if (this.props.nextTrack) {
                        this.preload(this.props.nextTrack)
                    }
                }
                break
            case "loadstart":
                bgEffects.wave()
                break
            case "canplaythrough":
                bgEffects.stop()
                if (!this.player.playing) {
                    let maybePromise = this.player.play()
                    if (maybePromise) maybePromise.catch( err => console.log(err) )
                }
                break
            case "ended":
                this.props.trackPlayed(this.props.track.ref)
                break
            case "error":
                console.log('audio error')
                console.log(this.player.error)
                bgEffects.flash('red')
                break
            case "playing":
                this.setState({ paused: this.player.paused})
                break
        }
    }

    preload(track) {
        let currentBufferSrc = this.buffer.getAttribute( "src" )
        let targetBufferSrc = scAuth.getStreamURL( track.streamURL )

        if (currentBufferSrc !== targetBufferSrc) {
            console.log(`preloading ${track.title}`)
            this.buffer.setAttribute( "src", targetBufferSrc )
        }
    }

    setSrc(track) {
        if (track) {
            let currentSrc = this.player.getAttribute( "src" )
            let targetSrc = scAuth.getStreamURL( track.streamURL )

            if (currentSrc !== targetSrc) {
                console.log(`playing ${track.title}`)

                let currentBufferSrc = this.buffer.getAttribute( "src" )
                if (currentBufferSrc === targetSrc ) {
                    let player = this.buffer
                    this.buffer = this.player
                    this.player = player

                    this.player.currentTime = this.player.buffered.end(0)
                    setTimeout( () => {
                        this.player.currentTime = 0
                    }, 1 )

                    this.removeEventHandlers( this.buffer )
                    this.addEventHandlers( this.player )

                    this.buffer.load()
                } else {
                    this.player.setAttribute("src", targetSrc )
                }

                let maybePromise = this.player.play()
                if (maybePromise) maybePromise.catch( err => console.log(err) )

                if (this.checkTrackPositionIntvl !== null) {
                    clearInterval( this.checkTrackPositionIntvl )
                    this.checkTrackPositionIntvl = setInterval( this.checkTrackPosition,
                                                               1000)
                }
            }
        }
    }

    _primePlay() {
        this.player.play()
        window.removeEventListener( "touch", this.primePlay )
        window.removeEventListener( "click", this.primePlay )
    }

    _togglePlay(ev) { 
        console.log('toggling play')

        ev.stopPropagation()

        if (this.player.paused) {
            this.player.play()
        } else {
            this.player.pause()
        }

        this.setState({
            paused : this.player.paused
        })
    }

    _seekPosition(pos) {
        this.player.currentTime = this.player.duration * pos
    }

    _skipTrack(ev) {
        ev.stopPropagation();
        this.props.trackSkip()
    }

    _handleKey(ev) {
        if (ev.target && ev.target.nodeName === "INPUT") return

        if (ev.keyCode === 32) {
            ev.preventDefault()
            ev.stopPropagation()
            this.togglePlay(ev)
        }
    }

    removeEventHandlers(node) {
        for (let event of this.events) {
            node.removeEventListener( event, this.boundEventHandler )
        }
    }

    addEventHandlers(node) {
        for (let event of this.events) {
            node.addEventListener( event, this.boundEventHandler )
        }
    }

    componentDidMount() {
        this.addEventHandlers(this.player)

        window.addEventListener( "keydown", this.handleKey )
        window.addEventListener( "touch", this.primePlay )
        window.addEventListener( "click", this.primePlay )

        this.setSrc(this.props.track)
    }

    componentDidUpdate(lastProps) {
        if (lastProps.track !== this.props.track) {
            this.setSrc(this.props.track)
        }
    }

    render() {
        console.log('audio render')
        let className = "controls"

        if (!this.props.nextTrack) {
            className += " solo"
        }

        return (
            <div className={ className }>
                <div className="buttons">
                    { this.props.nextTrack ? (
                        <button onClick={ this.skipTrack }>
                            <img src="/static/images/ic_skip_next_black_24px.svg"/>
                        </button>
                        )
                        : null
                    }
                    { this.props.track ? 
                        <button onClick={ this.togglePlay }>{
                            this.state.paused ? <img src="/static/images/ic_play_circle_filled_black_24px.svg" />
                            : <img src="/static/images/ic_pause_circle_outline_black_24px.svg" />
                        }</button>
                        : null
                    }
                </div>
                { (!this.props.closed && this.props.track && this.player) ?
                    <Seeker player={ this.player } /> 
                    : (console.log('no seeker') && null )
                }
            </div>
        )
    }
}

AudioPlayer.propTypes = {
    trackPlayed : React.PropTypes.func.isRequired
}

export default AudioPlayer
