import React, { Component } from "react"

import LinkIcon from "./LinkIcon"
import Selector from "./Selector"

const selectorImages = {
    tracks: "/images/tracks.svg",
    artists: "/images/artists.svg",
    channels: "/images/channels.svg"
}

class Navigator extends Component {
    constructor(props) {
        super(props)

        this.state = {
            paths : {
                tracks: {
                    activeSub : "feed",
                    subDomains : ["feed","favorites"]
                },
                artists : {
                    activeSub: "feed",
                    subDomains : ["feed","favorites"]
                },
                channels : {}
            }
        }
    }

    navigate(domain, sub) {
        if (domain in this.state.paths) {
            this.setState({
                paths : Object.assign({}, this.state.paths, {
                    [domain] : Object.assign({}, this.state.paths[domain], {
                        activeSub : sub
                    })
                })
            })
        }
    }

    findActiveSubDomains() {

        let paths = this.state.paths

        for (let domain in paths) {
            if ( this.props.location.pathname.startsWith(`/${domain}`) ) {
                let subDomains = paths[domain].subDomains

                if (subDomains) return { domain, subDomains }
                else return false
            }
        }

        return false
    }

    componentWillMount() {
        let path = window.location.pathname.split("/")

        this.navigate(path[1], path[2])
    }

    makeSubSelectors({domain, subDomains}) {
        if (!domain) return null
        let active = `/${domain}/${this.state.paths[domain].activeSub}`

        return subDomains.map( subDomain => {
            let path = `/${domain}/${subDomain}`
            let isActive = path === active

            return (
                <Selector 
                    key={ path } 
                    to={ path } 
                    isActive={ isActive }
                    onClick={ this.navigate.bind(this, domain, subDomain) }
                >{ subDomain }</Selector>
            )
        })
    }

    makeMainSelectors(domains) {
        if (!domains) return false

        let paths = this.state.paths

        return domains.map( domain => {
            let activeSub = paths[domain].activeSub || ""

            let path = `/${domain}/` + activeSub

            let active = this.props.location.pathname.startsWith(`/${domain}`)

            return (
                <Selector 
                    key={ path } 
                    to={ path } 
                    isActive={ active }
                    onClick={ this.navigate.bind(this, domain, activeSub) }
                    image={ selectorImages[domain] }
                >{ domain }</Selector>
            )
        })

    }

    render() {
        let subTabBar = null

        let domains = Object.keys(this.state.paths)

        let subDomains = this.makeSubSelectors( this.findActiveSubDomains() )

        if (subDomains) {
            subTabBar = (
                <ul className="tab-bar sub">
                    { subDomains }
                </ul>
            )
        } else console.log('no subs')


        return (
            <div>
                <ul key={ "mainTabBar" } className="tab-bar">
                    <LinkIcon to="/" id="homeLinkIcon" 
                        img="/images/home.svg" 
                        onClick={ this.navigate.bind(this, null) } />
                    <div className="main-selectors">
                        { 
                            this.makeMainSelectors(domains)
                        }
                    </div>
                    <a onClick={ this.props.openMenu }
                        className="icon"><img src="/images/menu.svg"/></a>
                </ul>
                { subTabBar }
            </div>
        )
    }
}

Navigator.propTypes = {
    openMenu : React.PropTypes.func.isRequired
}

export default Navigator
