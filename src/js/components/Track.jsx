import React from "react"
import { Link } from "react-router"

import Image from "./Image"
import TagList from "./TagList"
import ItemInfo from "./ItemInfo"

function Track(props, context) {
    let track = props.item;

    let parentRef = { trackRef : props.item.ref }

    return (
        <div className={ "track item" } >
            <Image 
                    className="track" 
                    onClick={ context.trackPlay.bind(null, track.ref, 
                                                     props.listName, props.index) }
                    bigImage={ props.bigImage }
                    src={ track.imageURL || "/images/waveform.svg" } />
            <ItemInfo
                domain="tracks"
                parentRef={ parentRef } 
                >
                <Link className="name" to={ `/info/artist/${ track.artistRef }` }>
                    <div>{ track.artist }</div></Link>
                <br />
                <Link className="title" to={ `/info/track/${ track.ref }` }>
                    <div>{ track.title }</div></Link>
                { track.tagRefs ? 
                    <TagList 
                        parentRef={ parentRef }
                        parentDomain="tracks"
                        tagRefs={ track.tagRefs } 
                        /> 
                    : null }
            </ItemInfo>
        </div>
    )
}

Track.propTypes = {
    item : React.PropTypes.object.isRequired
}

Track.contextTypes = {
    trackPlay : React.PropTypes.func.isRequired
}

export default Track
