import React from "react"

function loaded(ev) {
    ev.target.classList.remove("notloaded")
}

function Image(props) {
    return (
        <img className={ props.className + " notloaded" } 
            onLoad={ loaded } 
            onClick={ props.onClick }
            src={ props.bigImage ? props.src.replace("large","t300x300") : props.src } />
    );
}

Image.propTypes = {
    bigImage : React.PropTypes.bool
}

export default Image;
