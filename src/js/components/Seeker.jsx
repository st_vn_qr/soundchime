import React, { Component } from "react"

class Seeker extends Component {
    constructor(props) {
        super(props)

        let duration = this.props.player.duration / 100

        this.state = {
            position: (this.props.player.currentTime / duration) || 0,
            buffered: this.props.player.buffered.length ?
                this.props.player.buffered.end(0) / duration : 0
        }

        this.updateSeeker = this._updateSeeker.bind(this)
    }

    _updateSeeker() {
        let duration = this.props.player.duration / 100

        this.setState({
            position: (this.props.player.currentTime / duration) || 0,
            buffered: this.props.player.buffered.length ?
                this.props.player.buffered.end(0) / duration : 0
        })
    }

    componentDidMount() {
        this.update = setInterval( this.updateSeeker, 500 )
    }

    componentWillUnmount() {
        clearInterval( this.update )
    }

    render() {
        let thumbStyle = {
            left: this.state.position + "%"
        }

        let bufferedStyle = {
            width: this.state.buffered + "%"
        }

        console.log('seeker render')
        return (
            <div className="seeker">
                <div className="bar">
                    <div className="buffer" style={ bufferedStyle } />
                    <div className="thumb" style={ thumbStyle } />
                </div>
            </div>
        )
    }
}

Seeker.propTypes = {
    player : React.PropTypes.object.isRequired
}

export default Seeker
