import React, { Component } from "react"

class List extends Component {
    constructor(props) {
        super(props);

        if (props.requestList && props.retrieveResource) {
            let requestAction = props.requestList[props.params.list]

            if (requestAction) {
                this.request = props.retrieveResource.bind(null, 
                                               requestAction.type, 
                                               requestAction.platform ) 
            }
        }

        this.boundHandleScroll = this.handleScroll.bind(this)
    }

    handleScroll() {
        if ( window.scrollY !== 0 && (document.body.scrollTop + document.body.clientHeight) >= document.body.scrollHeight) {
            console.log('requesting from scroll')
            this.requestUpdate()
        }
    }

    requestUpdate() {
        console.log('requesting update')
        if (this.request) this.request()
    }

    componentDidMount() {
        console.log('list mounted')

        let scroll = (this.context.scroll[this.props.domain] && 
            this.context.scroll[this.props.domain][this.props.params.list]) || 0

        window.scrollTo(0, scroll)
        window.addEventListener( "scroll", this.boundHandleScroll )

        if (this.props.itemsFromState.processedItems.length === 0) {
            console.log('requesting from mount')
            this.requestUpdate()
        }
    }

    componentDidUpdate(lastProps) {
        console.log('list updated')

        if (this.props.requestList && this.props.retrieveResource) {
            let requestAction = this.props.requestList[this.props.params.list]

            if (requestAction) {
                this.request = this.props.retrieveResource.bind(null, 
                                               requestAction.type, 
                                               requestAction.platform ) 
            }
        }

        if (this.props.itemsFromState.processedItems.length === 0) {
            console.log('requesting from update')
            this.requestUpdate()
        }

        if (this.props.params.list !== lastProps.params.list) {

            let scroll = (this.context.scroll[this.props.domain] && 
                this.context.scroll[this.props.domain][this.props.params.list]) || 0

            window.scrollTo(0, scroll)

        }
    }

    componentWillUpdate(nextProps) {
        if (nextProps.params.list !== this.props.params.list) {
            this.context.reportScroll(this.props.domain, this.props.params.list, 
                                      window.scrollY)
        }
    }

    componentWillUnmount() {
        this.context.reportScroll(this.props.domain, this.props.params.list, 
                                  window.scrollY)

        window.removeEventListener( "scroll", this.boundHandleScroll )
    }


    shouldComponentUpdate(nextProps, nextState) {
        let originalChanged = (
                nextProps.itemsFromState.originalItems !== 
                    this.props.itemsFromState.originalItems
        );

        let listenObjectChanged = (
            this.props.itemsFromState.listenObject && 
                this.props.itemsFromState.listenObject !== 
                    nextProps.itemsFromState.listenObject
        ) || false

        let stateChanged = this.state !== nextState

        let listChanged = this.props.params.list !== nextProps.params.list

        return originalChanged || listenObjectChanged || stateChanged || listChanged
    }

    render () {
        console.log('rendering list')


        return (
            <div className="tab">
                <div className={ "list " + this.props.domain }>{ 
                        this.props.itemsFromState.processedItems.map( (item,ind) => {
                            return (
                                <this.props.itemComponent 
                                    key={ item.ref + ind } item={ item } 
                                    selectItem={ this.props.selectItem }
                                    selected={ this.props.selected }
                                    index={ ind }
                                    listName={ this.props.params.list }
                                    /> 
                            )
                        })
                    }
                </div>
                { this.props.children }
            </div>
        );
    }
}

List.contextTypes = {
    reportScroll : React.PropTypes.func.isRequired,
    scroll :React.PropTypes.object.isRequired
}


List.propTypes = {
    domain : React.PropTypes.string.isRequired,
    params : React.PropTypes.object.isRequired,
    itemsFromState : React.PropTypes.shape({
        originalItems: React.PropTypes.array,
        processedItems: React.PropTypes.array,
        listenObject : React.PropTypes.object
    })
}

export default List
