import { Link } from "react-router"

function Icon(props) {
    return(
        <Link {...props} className="icon">
            <img src={ props.img } />
        </Link>
    )
}

export default Icon
