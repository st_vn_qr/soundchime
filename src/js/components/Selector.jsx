import { Link } from "react-router";

function Selector(props) {
    return (
        <li className={ "selector" + ( props.isActive ? " active" : "" )  }
            onClick={ props.onClick}>
            { props.image ? <img src={ props.image } /> : null }
            <Link activeClassName="active" to={ props.to }>
                { props.children}
            </Link>
        </li>
    )
}

export default Selector
