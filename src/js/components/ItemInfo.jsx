import React, { Component } from "react"

class ItemInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            hover: false
        }

        this.selectInfo = Object.assign({ domain: this.props.domain }, 
                                        this.props.parentRef )
    }

    hover() {
        this.context.selectHover(this)
    }

    unhover(){
        this.setState({
            hover: false
        })
    }

    render() {

        let className = "item-info"
        if (this.state.hover) className += " hover"

        return (
            <div 
                className={ className }
                onClick={ this.context.selectItem.
                    bind(null, this.selectInfo) }
                onMouseEnter={ this.hover.bind(this) }
                onMouseLeave={ this.unhover.bind(this) }
            >{ this.props.children }</div>
        )
    }
}

ItemInfo.contextTypes = {
    selected : React.PropTypes.object,
    selectItem : React.PropTypes.func,
    selectHover : React.PropTypes.func
}

export default ItemInfo
