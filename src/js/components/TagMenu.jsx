import React, { Component } from "react"

import TagMenuList from "./TagMenuList"

class TagMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newTagName : ""
        }
    }

    addTag(ev) {
        ev.preventDefault()
        ev.stopPropagation()
        console.log('add tag')
        this.props.addTag( this.state.newTagName );
        this.setState({
            newTagName: ""
        });
    }

    onChange(ev) {
        this.setState({
            newTagName: ev.target.value
        })
    }

    stopProp(ev) {
        ev.stopPropagation()
    }

    render() {
        let activeTagRef, inner
        let menuClass = "menu"
        if (this.props.closed) menuClass += " closed"

        if (this.context.selected && this.context.selected.domain === "tagmenu" &&
            this.context.selected.tagRef) activeTagRef = this.context.selected.tagRef

        if (activeTagRef) {
            inner = (
                <div>
                    Tagging: { activeTagRef }
                    <button onClick={ this.context.selectItem.bind(null, null) 
                        }>{ "[ < ]" }</button>
                </div>
            )
        } else {
            inner = (
                <div>
                    <form onSubmit={ this.addTag.bind(this) }>
                        <input onChange={ this.onChange.bind(this) } 
                            value={ this.state.newTagName }
                            placeholder="[ + ]" />
                    </form>
                    <TagMenuList 
                        tagCounts={ this.props.tagCounts } />
                    <button onClick={ this.props.closeMenu }>[ x ]</button>
                </div>
            )
        }

        return (
            <div
                id="tagMenu" 
                key="tagMenu"
                className={ menuClass }
                onClick={ this.stopProp }
            >{ inner }</div>
        )
    }
}

TagMenu.contextTypes = {
    selected : React.PropTypes.object,
    selectItem : React.PropTypes.func
}
export default TagMenu;
