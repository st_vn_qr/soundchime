import React from "react"

import TagList from "./TagList"

function Channel(props, context) {
    let channel = props.channel

    let selectInfo = {
        domain : "channels",
        channelRef : channel.ref
    }

    return (
        <div 
            className="channel"
            onClick={ context.selectItem.bind(null, selectInfo ) }>
            <div className="name">{ channel.ref }</div>
            { 
                channel.tagRefs ? 
                    <TagList
                        tagRefs={ channel.tagRefs }
                        parentRef={ { channelRef : channel.ref } }
                        parentDomain="channels" />
                    : null
            }
            <div>{ channel.trackListRefs.map( list => <div key={list}
                >{ list }</div> ) 
            }</div>
        </div>
    )
}

Channel.contextTypes = {
    selectItem : React.PropTypes.func
}

export default Channel
