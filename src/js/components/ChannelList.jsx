import Channel from "./Channel"

export default function(props) {
    console.log('channel list update')
    console.log(props.channels)
    let channels = [];

    for ( let channel in props.channels ) {
        channels.push(
            <Channel key={ channel } channel={ props.channels[channel] } />
        );
    }

    return ( <div className="channels" >{ channels }</div>)
}
