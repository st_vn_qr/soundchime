import { Component } from "react"

class ChannelCreate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name : "",
            tags : "",
            favorites : false
        }
    }

    create(ev) {
        ev.preventDefault();
        console.log('adding channel')


        if (this.state.name.length ) {
            let trackListRefs
            if ( this.state.favorites ) {
                trackListRefs = ["favorites"]
            } else {
                trackListRefs = ["feed", "favorites"]
            }

            this.props.create( this.state.name,
                              this.state.tags.length ? this.state.tags : null, 
                              trackListRefs )
        }

        this.setState({
            name: "",
            tags : "",
            favorites: false
        })
    }

    render() {
        return (
            <div class="channel create" onClick={ noop }>
                <form onSubmit={ this.create.bind(this) }>
                    <input onChange={ updateName.bind(this) } 
                        value={ this.state.name } placeholder="name" />
                    <input onChange={ updateTags.bind(this) } 
                        value={ this.state.tags } placeholder="tags"/>
                    <div>
                        <input type="checkbox" 
                            onChange={ checkFavoritesOnly.bind(this) }
                            checked={ this.state.favorites }/>
                        Only favorites
                    </div>
                    <input type="submit" value="add" />
                </form>
            </div>
        )
    }
}

function noop(ev) {
    ev.stopPropagation()
}


function checkFavoritesOnly(ev) {
    this.setState({ favorites: ev.target.checked })
}
function updateName(ev) {
    this.setState({ name: ev.target.value })
}

function updateTags(ev) {
    this.setState({ tags: ev.target.value })
}

export default ChannelCreate
