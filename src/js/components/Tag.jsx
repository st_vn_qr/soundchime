import React, { Component } from "react"

class Tag extends Component {
    constructor(props) {
        super(props)
        this.state = {
            confirm: false
        }

        this.boundConfirm = this.confirm.bind(this)

        this.selectInfo = Object.assign({

            domain: "tags",
            tagRef : this.props.tag,
            parentDomain: this.props.parentDomain

        }, props.parentRef)
    }

    clickHandler(ev) {
        this.context.selectItem( this.selectInfo, ev )

        if (!this.state.confirm) {
            this.context.selectListener.add( this.boundConfirm )
            this.setState({confirm: true})
        }
    }

    confirm(selected) {
        this.context.selectListener.remove( this.boundConfirm)

        if (selected !== this.selectInfo) {
            this.setState({confirm: false})
            console.log('unconfirm')
        }

    }

    render() {
        let classString = "tag"
        if (this.props.auto) classString += " auto";
        if (this.state.confirm) classString += " confirm";
            
        return (
            <div 
                className={ classString } 
                onClick={ this.clickHandler.bind(this) }
            >{ this.props.tag }</div>
        )
    }
}

Tag.propTypes = {
    parentRef : React.PropTypes.object.isRequired,
    parentDomain: React.PropTypes.string.isRequired
}

Tag.contextTypes = {
    selectItem : React.PropTypes.func.isRequired,
    selectListener : React.PropTypes.object.isRequired
}

export default Tag
