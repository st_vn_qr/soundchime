import React from "react"

function TagMenuList(props, context) {

    let tags = Object.keys(props.tagCounts).map( tagRef => {
        let selectInfo = {
            domain : "tagmenu",
            tagRef
        }

        return (
            <div 
                className="tag"
                key={ tagRef } 
                onClick={ context.selectItem.bind( null, selectInfo ) }
            >{ tagRef }</div>
        )
    })

    return (
        <div className="tags">{ tags }</div> 
   )
}

TagMenuList.contextTypes = {
    selected : React.PropTypes.object,
    selectItem : React.PropTypes.func
}

export default TagMenuList
