import React from "react"
import Track from "./Track"
import Artist from "./Artist"

function TrackInfo(props) {
    console.log(props.track.description)
    window.scrollTo(0,0)

    return (
        <div className="info">
            <div>
                <a href={props.track.permalink} target="_blank">
                    <img className="logo-text" 
                        src="/static/images/sc_logo_text.png" />
                </a>
            </div>
            <br />
            <Track item={ props.track } />
            <br />
            Posted by:
            <Artist item={ props.artist } />
            <br />
            <div className="description">{ parseLinks(props.track.description) || "( No description )" }</div>
        </div>
    )
}

const atRe = /\B(?:\@([a-z0-9_-]+))(?![\.a-z\-])/ig

const urlRe = /(?:(http(?:s)?\1):\/\/)?(?:www\.)?([a-z0-9\.]+)\.(\w{2,})\/?([a-z0-9./?=%+_-]+)?/ig

function parseLinks(string) {
    if (!string) return null
    let res = [];
    let flag = 0;
    let linkBuilder;

    let items = string.split(urlRe);

    let len = items.length;

    for (let i=0; i < len; i++) {
        switch (flag) {
            case 0:
                flag++;
                res = res.concat( parseAts( items[i] ) )
                break
            case 1:
                flag++;
                linkBuilder = { https: items[i] }
                break
            case 2:
                flag++;
                linkBuilder.domain = items[i]
                break
            case 3:
                flag++;
                linkBuilder.tld = items[i]
                break
            case 4:
                flag = 0;
                linkBuilder.path = items[i]

                var link = linkBuilder.domain + "." + linkBuilder.tld + "/" +
                    (linkBuilder.path || "");

                    var display = (linkBuilder.http || "" ) + link;

                    res.push(
                        <a href={ `${(linkBuilder.http || "http")}://${ link }` }
                            target="_blank" key={ link }>{ display }</a>
                    )
        }
    }

    return res;
    // split string according to matches
    // if 'javascript:' in potential link, toss
    // interlace original and href elem
    // res.push
}

function parseAts(string) {
    if (!string) return null
    let res = [];
    let items = string.split(atRe);

    let len = items.length;

    let match = false;
    for (let i=0; i<len; i++ ) {
        if (match) {
            match = false;
            res.push(<a href={ `https://www.soundcloud.com/${items[i]}` }
                         target="_blank"
                         key={ items[i] }
                     >{ `@${items[i]}` }</a>)
        } else {
            match = true;
            res.push(items[i])
        }
    }

    console.log(res)
    return res

}


TrackInfo.contextTypes = {
    selectItem : React.PropTypes.func.isRequired
}

export default TrackInfo
