import Artist from "./Artist"
import { Link } from "react-router"

export default function(props) {
    let lists = props.lists

    let tracks = Object.keys(lists).map( key => {
        return (
            <div>In { key }: 
                <br />
                { 
                    lists[key].map( track => {
                        return (
                            <div><Link key={track.ref + key} 
                                to={ `/info/track/${track.ref}`}
                                >{track.title}</Link></div>
                        )
                    })
                }
            </div>
        )
    })

    return (
        <div className="info">
            <div>
                <a href={ props.artist.permalink } target="_blank"><img src="/static/images/sc_logo_text.png"/></a>
            </div>
            <br />
            <Artist item={ props.artist } />
            <br />
            <div>{ tracks }</div> 
        </div>
    )
}
