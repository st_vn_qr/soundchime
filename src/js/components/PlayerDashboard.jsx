import React, { Component } from "react"
import Channel from "./Channel"
import Track from "./Track"
import AudioPlayer from "./AudioPlayer"
import PoweredBySoundCloud from "./PoweredBySoundCloud"

class PlayerDashboard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            closed : true,
            big : window.screen.width > 630
        }

        this.toggleClosed = this._toggleClosed.bind(this)
    }

    _toggleClosed() {
        this.setState({
            closed : !this.state.closed
        })
    }
    
    render() {
        if (!this.props.channel) return null

        let pastTracks, track, nextTrack

        if (!this.state.closed) {
            track = this.props.track ? (
                <div key="track" className="player-track">Present
                    <Track bigImage={ this.state.big } item={ this.props.track } /> 
                </div>
            ) : null 

            nextTrack = this.props.nextTrack ? (
                <div key="next" className="player-track">Future
                    <Track bigImage={ this.state.big } item={ this.props.nextTrack } />
                </div>
            ) : null 

            pastTracks = this.props.pastTracks ? (
                [<div key="past" className="past">Past</div>].concat(
                    this.props.pastTracks.map( (track, ind) => (
                        <div key={ "past" + ind } className="player-track">
                            <Track 
                                bigImage={ this.state.big } 
                                item={ track } />
                        </div>
                    ))
                )
            ) : null

        } else {
            track = null
            nextTrack = null
        }

        let className = "player" + (this.state.closed ? " closed" : "")

        console.log('dash render')

        document.title = `${this.props.channel.ref}`

        if (this.props.track) document.title += 
            `: ${this.props.track.artist} - ${this.props.track.title}`

        return (
            <div className={ className } onClick={ this.toggleClosed }>
                { (!this.props.track && !this.props.nextTrack) ? 
                    (<div>no tracks</div>) : 
                        (!this.props.nextTrack) ? (<div>last one</div>) : null }

                { this.state.closed ? (
                    this.props.track ? (
                        <img className="track" src={ this.props.track.imageURL } />
                    ) : null
                ) :
                    <div className="player-info">
                        <Channel channel={ this.props.channel } />
                        { track }
                        { nextTrack }
                        { pastTracks }
                    </div>
                }
                <AudioPlayer {...this.props} 
                    key="audioPlayer"
                    closed={ this.state.closed } />
                { this.props.track ? <PoweredBySoundCloud /> : null }
            </div>
        )
    }
}


PlayerDashboard.contextTypes = {
    selectItem : React.PropTypes.func.isRequired
}

export default PlayerDashboard
