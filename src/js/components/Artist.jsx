import React from "react"
import { Link } from "react-router"

import Image from "./Image"
import TagList from "./TagList"
import ItemInfo from "./ItemInfo"

function Artist(props) {
    let artist = props.item;

    let parentRef = { artistRef : artist.ref }
    

    return (
        <div className="artist item">
            <Image 
                className="track" 
                src={ artist.imageURL || "/images/waveform.svg"} />
            <ItemInfo
                domain="artists"
                parentRef={ parentRef }
            ><Link to={ `/info/artist/${ artist.ref }` }>
                    <div className="name">{ artist.name }</div></Link>
                <div className="count">{ (artist.count > 1 ) ? 
                    `x${artist.count}` : null }</div>
                { artist.tagRefs ? 
                    <TagList 
                        parentRef={ parentRef }
                        parentDomain="artists"
                        tagRefs={ artist.tagRefs } /> 
                    : null }
            </ItemInfo>
        </div>
    )
}

Artist.propTypes = {
    item : React.PropTypes.object.isRequired
}

export default Artist
