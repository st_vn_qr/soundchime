import React from "react"
import Tag from "./Tag"

function TagList(props) {
    return (
        <div className="tags">{ 
             props.tagRefs.map( tagRef => {
                return (
                    <Tag 
                        key={ tagRef }
                        tag={ tagRef }
                        parentRef={ props.parentRef }
                        parentDomain={ props.parentDomain }
                    />
                )
            })
        }</div>
    )
}

TagList.propTypes = {
    tagRefs : React.PropTypes.array.isRequired,
    parentRef : React.PropTypes.object.isRequired,
    parentDomain: React.PropTypes.string.isRequired
}



export default TagList
