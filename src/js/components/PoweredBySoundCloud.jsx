export default function() {
    return(
        <div className="powered-by">
            <img src="/static/images/sc_powered_logo.png" />
        </div>
    )
}
