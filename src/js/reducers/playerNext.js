import {
    TRACK_TAG_ADD,
    TRACK_TAG_REMOVE,
    ARTIST_TAG_ADD,
    ARTIST_TAG_REMOVE,
    CHANNEL_TAG_ADD,
    CHANNEL_TAG_REMOVE,
    CHANNEL_SELECT,
    TRACK_PLAYED,
    TRACK_PLAY,
    TRACK_SKIP,
    API_RESOURCE_RETRIEVED
} from "../actions"

function matchChannelTrack(channelTagRefs, trackTagRefs=[], artistTagRefs=[]) {
    let tagRefs = trackTagRefs.concat( artistTagRefs )

    tagRefs.map( t => t.toLowerCase() )

    for (let i=0; i < channelTagRefs.length; i++) {
        if (tagRefs.indexOf(channelTagRefs[i]) === -1) {
            return false
        }
    }

    return true
}

function findNextTrackRef(state, channelTagRefs, tracksList, startIndex=0) {

    for (let i=startIndex; i<tracksList.length; i++) {

        let track, artist
            track = state.tracks[ tracksList[i] ]

        if (!track) continue

            artist = state.artists[ track.artistRef ]
        
        if (channelTagRefs.length === 0) {

            return [ track.ref,  i ]

        } else if (track.tagRefs || artist.tagRefs) {

            if ( matchChannelTrack(channelTagRefs, track.tagRefs, artist.tagRefs) ) {

                return [ track.ref, i ]
            }
        }
    }

    return [ null, -1 ]
}

function nextPlayer(state, playerState, goNext) {
    let currentTrackRef, nextTrackRef, nextIndex
    let channel, tracksList, startIndex


    channel = state.channels[ playerState.currentChannelRef ]

    tracksList = channel.trackListRefs.map( listRef => state.lists.tracks[listRef] )
    tracksList = tracksList.reduce( (resList, list) => {
        if (list) {
            return resList.concat(list)
        } else return resList
    }, [])



    currentTrackRef = playerState.currentTrackRef
    nextIndex = playerState.nextIndex

    if (!currentTrackRef) {
        [currentTrackRef, nextIndex] = findNextTrackRef(state, channel.tagRefs,
                                                        tracksList, 0)
        goNext = 1;
    }


    startIndex = nextIndex + goNext;

    [nextTrackRef, nextIndex] = findNextTrackRef(state, channel.tagRefs, 
                                                     tracksList, startIndex)

    if (nextIndex === -1) {
        [nextTrackRef, nextIndex] = findNextTrackRef(state, channel.tagRefs, 
                                                         tracksList, 0) 
    }

    return Object.assign({}, state, {
        player : Object.assign({}, playerState, {
            nextTrackRef, nextIndex, currentTrackRef
        })
    })
}

export default function(state={}, action) {
    let playerState, firstChannelRef, goNext = 0

    switch (action.type) {
        case TRACK_PLAY:
            if (state.player.currentChannelRef) {
                return state
            } else {
                firstChannelRef = Object.keys(state.channels)[0]
                if (firstChannelRef) {

                    let nextIndex = action.index;
                    if (action.listName === "favorites") {
                        nextIndex += state.lists.tracks["feed"] ? state.lists.tracks["feed"].length : 0
                    }

                    playerState = Object.assign({}, state.player, { 
                        currentChannelRef: firstChannelRef,
                        nextIndex
                    })
                }
            }
        case TRACK_PLAYED:
        case TRACK_SKIP:
            goNext = 1
        case TRACK_TAG_ADD:
        case TRACK_TAG_REMOVE:
        case ARTIST_TAG_ADD:
        case ARTIST_TAG_REMOVE:
        case CHANNEL_TAG_ADD:
        case CHANNEL_TAG_REMOVE:
        case CHANNEL_SELECT:
        case API_RESOURCE_RETRIEVED:

            if (playerState === undefined) playerState = state.player

            if (playerState.currentChannelRef && state.lists.tracks) {
                return nextPlayer( state, playerState, goNext )
            }

            return state


        default:
            return state
    }
}
