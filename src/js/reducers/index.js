import { combineReducers } from "redux"

import tracks from "./tracks"
import channels from "./channels"
import artists from "./artists"
import api from "./api"
import lists from "./lists"
import tags from "./tags"
import player from "./player"

import playerNext from "./playerNext"

const combined = combineReducers({
    api,
    tracks,
    channels,
    artists,
    lists,
    tags,
    player
});

function reducer(state, action) {
    return playerNext( combined(state, action), action)
}

export default reducer;
