import {
    API_RESOURCE_RETRIEVED,
    ARTIST_TAG_ADD,
    ARTIST_TAG_REMOVE
} from "../actions"

import {
    TRACK_FEED,
    TRACK_FAVORITES
} from "../api/resources"

function addArtist(state, artist) {

    if ( state[artist.ref] === undefined ) {

        return Object.assign( state, { [artist.ref] : artist } )

    } else return state
}

function addArtistTrack(state, track) {

    let artist = state[ track.artistRef ];

    if ( artist.trackRefs === undefined ) {

        return Object.assign( state, {

            [ track.artistRef ] : 

                Object.assign( {}, artist, {

                    trackRefs : [ track.ref ]
                })
        })
    }
    if ( artist.trackRefs.indexOf( track.ref ) === -1 ) {

        return Object.assign( state, {

            [ track.artistRef ] : 

                Object.assign( {}, artist, {

                    trackRefs : artist.trackRefs.concat( track.ref )
                })
        })
    } else return state
}

const artists = (state = {}, action) => {
    let tagRefs, artist, add

    switch (action.type) {

        case API_RESOURCE_RETRIEVED:

            switch (action.resource) {
                case TRACK_FEED:
                case TRACK_FAVORITES:
                    return (
                        action.response.tracks

                            .reduce( addArtistTrack, action.response.artists

                                    .reduce( addArtist, Object.assign({}, state) ) 

                                   )
                    )
                default: return state
            }

        case ARTIST_TAG_ADD:
            add = true
        case ARTIST_TAG_REMOVE:

            artist = state[ action.artistRef ]
            tagRefs = artist.tagRefs
            
            if (add) {
                if (!tagRefs) tagRefs = []

                tagRefs = tagRefs.concat( action.tagRef )
            } else {
                if (!tagRefs) return state

                let ind = tagRefs.indexOf( action.tagRef )

                if (ind !== -1) {
                    tagRefs = tagRefs.slice(0, ind).concat( tagRefs.slice(ind+1) )
                } else {
                    return state
                }
            }

            return Object.assign({}, state, {
                [ action.artistRef] : Object.assign({}, artist, { tagRefs })
            })
        default: return state
    }
}

/*
 * These functions were used when artist.tags were objects like {ref, count, auto}
function findTagIndex(tagRefs, tagRef) {
    let index = tagRefs.length - 1;

    while(index + 1) {
        if (tagRef === tagRefs[index].tagRef) {
            break
        }

        index--;
    }


    return index
}

function getArtistRefFromTrackRef(state, trackRef) {
    for (let artistRef in state) {
        for (let _trackRef of state[artistRef].trackRefs) {
            if ( _trackRef === trackRef) return artistRef
        }
    }

    return false
}

*/

export default artists
