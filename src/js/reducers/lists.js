import {
    API_RESOURCE_RETRIEVED
} from "../actions"

import {
    TRACK_FEED,
    TRACK_FAVORITES
} from "../api/resources"


function addItems(state = {}, action, domain) {
    let listName;
    switch (action.resource) {

        case TRACK_FEED:

            listName = "feed";

        case TRACK_FAVORITES:

            if (!listName) {
                listName = "favorites"
            }

            var refs = action.response[domain].map( 

                domain === "tracks" ? 

                     ( track => { return track.ref } ) 

                     :   ( artist => { return artist.ref } )
                
            );

            var list = ( state[listName] || [] ).concat( refs );

            return Object.assign( {}, state, {
                [listName] : list
            });

        default: return state
    }
}

const lists = (state = {}, action) => {

    switch (action.type) {

        case API_RESOURCE_RETRIEVED:

            return Object.assign( {}, state, {
                tracks : addItems( state.tracks, action, "tracks" ),
                artists: addItems( state.artists, action, "artists" )
            });

        default: return state
    }
}

export default lists
