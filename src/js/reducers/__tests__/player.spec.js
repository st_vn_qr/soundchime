/* eslint-env mocha */
import * as playerActions from '../../actions/player'
import playerReducer from '../player'
import { mock_track, mock_tracks } from '../../__tests__/.mocks'

describe('player', function() {

    const initialState = playerReducer( undefined, {} )
    describe('initial state', function() {
        it('should have properties', function() {
            initialState.should.have.property('pastTrackRefs')
        })
    })

    const playState = playerReducer( initialState,
                                      playerActions.trackPlay( mock_track.ref ) )

    describe('trackPlay', function() {
        it('should set next track', function() {
            playState.should.have.property('currentTrackRef')

            playState.currentTrackRef
                .should.equal( mock_track.ref )
        })
    })


    const mockState_1 = Object.assign({}, playState, {
        nextTrackRef: mock_tracks[1].ref
    })

    const skippedState = playerReducer( mockState_1,
                                       playerActions.trackSkip() )

    describe('trackSkip', function() {
        it('should set currentTrackRef = nextTrackRef', function() {
            skippedState.should.have.property('nextTrackRef')
            skippedState.currentTrackRef.should.equal( mock_tracks[1].ref )
        })

        it('should not add skipped tracks to played list', function() {

            skippedState.should.have.property('pastTrackRefs')
            skippedState.pastTrackRefs.should.be.empty

        })
    })

    const playedState = playerReducer( playState,
                                      playerActions.trackPlayed( mock_track.ref ) )

    describe('trackPlayed', function() {
        it('should add track to pastTrackRefs', function() {
            playedState.should.have.property('pastTrackRefs')

            playedState.pastTrackRefs.should.have.length(1)

            playedState.pastTrackRefs[0]
                .should.be.equal( mock_track.ref )
        })
    })
})
