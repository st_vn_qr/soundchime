/* eslint-env mocha */
/* global should */

import * as tagActions from '../../actions/tags'
import tagReducer from '../tags'
import { mock_tracks } from '../../__tests__/.mocks'

describe('tags', function() {

    const initialState = tagReducer(undefined, {})

    describe('initial state', function() {
        it('should be empty', function() {
            should.exist(initialState)
            Object.keys(initialState).should.have.length(0)
        })
    })

    const tagAddState = tagReducer(initialState, tagActions.tagNew(

    ))

    describe('tagNew', function() {
        it('should add a new tag to tags domain', function() {
            
        })
    })

    describe('', function() {
        it('', function() {
        })
    })

    describe('', function() {
        it('', function() {
        })
    })
})
