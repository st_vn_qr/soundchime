import {
    TRACK_FEED,
    TRACK_FAVORITES
} from "../api/resources";

import { 
    API_RESOURCE_RETRIEVED,
    TRACK_TAG_ADD,
    TRACK_TAG_REMOVE
} from "../actions";

function newTrack(state, track) {
    if ( state[track.ref] === undefined ) {
        return Object.assign( state, {
            [track.ref] : Object.assign( {}, track )
        })
    } else {
        return state
    }
}

const tracks = (state = {}, action) => {

    let track, tagRefs, newState;

    switch (action.type) {
        case API_RESOURCE_RETRIEVED:

            switch (action.resource) {

                case TRACK_FEED:
                case TRACK_FAVORITES:
                    newState = Object.assign({}, state)
                    return action.response.tracks.reduce( newTrack, newState )

                default: return state;
            }

        case TRACK_TAG_ADD:
            track = state[action.trackRef];
            if (!track) return state

            tagRefs = track.tagRefs || [];

            if (tagRefs.indexOf( action.tagRef ) !== -1) {
                console.log('tag already exists')
                return state
            }

            tagRefs = tagRefs.length ? tagRefs.concat(action.tagRef) : [action.tagRef];

            return Object.assign( {}, state, {
                [action.trackRef] : Object.assign( {}, track, { tagRefs } ) } )
        case TRACK_TAG_REMOVE:
            track = state[action.trackRef];
            if (!track) return state

            tagRefs = track.tagRefs
            if (!tagRefs) return state

            var index = tagRefs.indexOf( action.tagRef );

            if ( index === -1) {
                console.log('tag doesnt exist')
                return state
            }

            tagRefs = tagRefs.slice(0, index).concat( tagRefs.slice( index+1 ) )

            return Object.assign( {}, state, {
                [action.trackRef] : Object.assign( {}, track, { tagRefs } ) } )
        

        default: return state;
    }
}

export default tracks;
