import {
    TRACK_PLAYED,
    TRACK_PLAY,
    TRACK_SKIP,
    CHANNEL_SELECT
} from "../actions"

const initialPlayerState = {
    pastTrackRefs : []
}

export default function(state=initialPlayerState, action) {
    let pastTrackRefs = state.pastTrackRefs

    switch (action.type) {
        case TRACK_PLAYED:
            pastTrackRefs = pastTrackRefs.concat(state.currentTrackRef)
        case TRACK_SKIP:
            return Object.assign({}, state, {
                currentTrackRef : state.nextTrackRef,
                pastTrackRefs
            });
        case TRACK_PLAY:
            return Object.assign({}, state, {
                currentTrackRef : action.trackRef
            })
        case CHANNEL_SELECT:
            return Object.assign({}, state, {
                currentChannelRef : action.channelRef
            })
        default: return state
    }
}
