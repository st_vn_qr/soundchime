import { 
    CHANNEL_NEW,
    CHANNEL_TAG_ADD,
    CHANNEL_TAG_REMOVE
} from "../actions"

import bgEffects from "../bgEffects"

const initial = {
    ["Play All"] : {
        ref : "Play All",
        trackListRefs: ["feed","favorites"],
        tagRefs: []
    },
    ["💕"] : {
        ref : '💕',
        trackListRefs: ["favorites"],
        tagRefs: []
    }
}

const channels = (state = initial, action) => {
    let channel, tags, ind;

    switch (action.type) {
        case CHANNEL_NEW:
            console.log( 'channels reducer' );
            console.log( action );

            if ( state[action.channelRef] === undefined ) {
                let tagRefs = action.tagRefs ? 
                        action.tagRefs.split(" ").map( s => s.toLowerCase())
                        : []
                
                return Object.assign( {}, state, {
                    [action.channelRef] : { 
                        ref: action.channelRef,
                        trackListRefs : action.trackListRefs || ["feed", "favorites"],
                        tagRefs
                    }
                })

            } else {
                bgEffects.flash("red");
                console.log("channel already there");
                return state
            }

        case CHANNEL_TAG_ADD:
            channel = state[action.channelRef]

            if (channel) {
                ind = channel.tagRefs.indexOf(action.tagRef)

                if ( ind === -1 ) {
                    tags = channel.tagRefs.concat(action.tagRef)

                    return Object.assign({}, state, {
                        [action.channelRef] : 
                            Object.assign({}, channel, { tagRefs: tags })
                    })
                }
            }

            bgEffects.flash("red");
            return state

        case CHANNEL_TAG_REMOVE:
            channel = state[action.channelRef]

            if (channel) {
                ind = channel.tagRefs.indexOf(action.tagRef)

                if ( ind !== -1 ) {
                    tags = channel.tagRefs.slice(0, ind)
                        .concat( channel.tagRefs.slice( ind + 1 ) )

                    return Object.assign({}, state, {
                        [action.channelRef] : 
                            Object.assign({}, channel, { tagRefs: tags })
                    })
                }
            }
            return state
        default:
            return state
    }
}

export default channels
