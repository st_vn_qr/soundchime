import { TAG_NEW } from "../actions"

function findIndex(state, tagRef) {
    let index = state.length - 1;

    while(index + 1) {
        if (tagRef === state[index].ref) {
            break
        }
        index--;
    }

    return index
}

const tags = function(state = [], action) {
    let index;
    switch (action.type) {
        case TAG_NEW:
            if (!action.tagRef || !action.tagRef.length) {
                return state
            }

            index = findIndex(state, action.tagRef.toLowerCase());

            if (index > 0) {
                console.log('tag already there')
                return state
            } else {
                console.log('adding tag')

                if (action.tagRef.indexOf(" ") === -1 ) {
                    return state.concat( { ref: action.tagRef } )
                } else {
                    return state.concat({ 
                        ref: action.tagRef.split(" ").join("")
                    })
                }
            }

        default:
            return state
    }
}

export default tags
