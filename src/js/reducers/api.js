import { 
    API_RESOURCE_RETRIEVED,
    API_RESOURCE_REQUEST,
    API_CONNECTION
} from "../actions"

import {
    READY,
    CONNECTING,
    RETRIEVING,
    ERROR
} from "../api/status"

import bgEffects from "../bgEffects"


function api(state = {}, action) {
    let status;

    switch ( action.type ) {
        case API_RESOURCE_REQUEST:
            status = RETRIEVING;
            bgEffects.animate();
            break

        case API_RESOURCE_RETRIEVED:
            status = READY;
            //bgEffects.images.addAllLoadedListener( () => bgEffects.stop() );
            bgEffects.stop()
            break

        case API_CONNECTION:
            if (action.status === CONNECTING) {
                bgEffects.color("yellow");
            } else if (action.status === READY) {
                bgEffects.flash("green");
            } else if (action.status === ERROR) {
                bgEffects.stop();
                bgEffects.color("red");
            }
            status = action.status;
            break

        default: return state
    }
    
    return Object.assign( {}, state,
        {
            [action.platform] : { status }
        }
    )
}

export default api
