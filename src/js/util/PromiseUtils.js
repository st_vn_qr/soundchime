class MultiPromise {
    constructor(promises) {
        this.promises = promises;
    }

    then(what) {
        this.promises = this.promises.map( (promise) => {
            return promise.then(what);
        });

        return this;
    }

    catch(what) {
        this.promises = this.promises.map( (promise) => {
            return promise.catch( what );
        });

        return this;
    }
}

function LogPromiseError(error) {
    if (typeof process === "undefined") {
        console.log(`\n%c${error.name}`, "color:red;font-weight:bold;font-size:large;");
    } else {
        console.log(`\nError in promise: ${error.name}`);
    }

    console.log(error.stack);
    console.log("");
}

export { MultiPromise, LogPromiseError };
