export const checkProps = (obj, props) => {
    var missingProps = [];

    let objType = typeof obj;

    if ( objType !== "object" ) {

        if ( objType !== props ) {

            console.log(`${obj} was a ${objType}, expected ${props}`);

            throw new TypeError("Property mismatch");
        }
    }

    for ( let p in props ) {

        if ( !obj.hasOwnProperty( p ) ) {
            missingProps.push( p );
            continue;
        }

        let type = typeof obj[p];

        if ( typeof props[p] === "string" ) {

            if ( type !== props[p] ) {
                missingProps.push( p );
            }

        } else if ( Array.isArray( props[p] ) ) {

            if ( props[p].indexOf( type ) === -1 ) {
                missingProps.push( p );
            }
        }
    }

    if ( missingProps.length ) {

        console.log("! > Properties mismatched");
        console.log(obj);

        for ( let p of missingProps ) {

            if ( !obj.hasOwnProperty(p) ) {
                console.log(`missing: ${p}`);
            } else {
                console.log(`${p} was ${typeof obj[p]}, expected ${props[p]}`);
            }
        }


        throw new TypeError("Property mismatch");
    } else return true
}
