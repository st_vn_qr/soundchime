const PLATFORMS = Object.freeze({
    SoundCloud: "SoundCloud",
    MockPlatform: "MockPlatform"
});

const STATUS = Object.freeze({
    notConnected: 'not connected',
    connected: 'connected',
    pending: 'pending',
    disconnected: 'disconnected',
    error: 'error'
});

export { PLATFORMS, STATUS };
