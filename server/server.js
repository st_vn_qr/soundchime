/*eslint-env node, es6*/
"use strict"
var express = require("express");
var morgan = require("morgan");
var path = require("path");
var bodyParser = require("body-parser");
var compression = require("compression");

const origin = path.resolve(process.cwd(), "dist/");

var app = express();


let index = "index.html"

let production = process.env.NODE_ENV === "production"
if (production) index = "index.min.html"

app.use( morgan() );
app.use( compression() );
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({ extended: true }))

app.use( express.static(origin) );

app.get( "*", function( req, res ) {
    let file = path.join( origin, index )
    res.sendFile( file );
});

app.post( "*", function( req ) {
    console.log(`POST - ${req.url}`)
    console.log(req.body)
});


var PORT = process.argv[2] || 8080;

app.listen(PORT, function() {
    console.log(`Express hosting ${origin} on port ${PORT}`);
    if (production) console.log('in production')
});
